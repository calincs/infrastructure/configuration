#!/bin/bash

# Run this script straight from the GitLab repo with:
# curl -s https://gitlab.com/calincs/infrastructure/configuration/-/raw/master/resources/prepare_node.sh | bash
# or
# wget -O - https://gitlab.com/calincs/infrastructure/configuration/-/raw/master/resources/prepare_node.sh | bash

# Fix for too many DNS servers
echo "Updating node configs..."
sudo bash -c 'cat << EOF >> /etc/netplan/50-cloud-init.yaml
            dhcp4-overrides:
                use-dns: false
            nameservers:
                search: [arbutus]
                addresses: [192.168.211.2, 192.168.211.5]
EOF'
sudo netplan --debug apply

#Fix for Longhorn volumes not mounting
sudo bash -c 'cat << EOF >> /etc/multipath.conf
blacklist {
    devnode "^sd[a-z0-9]+"
}
EOF'

# Fix for too many files open
sudo bash -c 'cat << EOF >> /etc/sysctl.d/60-fs-inotify.conf
fs.inotify.max_queued_events = 32768
fs.inotify.max_user_instances = 512
fs.inotify.max_user_watches = 524288
EOF'
sudo sysctl --system

# Move k8s to the mounted volume on the node
echo "Stopping RKE2..."
cd /var/lib/rancher
sudo rke2-killall.sh
echo "Moving RKE2 folder to /mnt..."
sudo cp -a rke2 /mnt/rke2
sudo rm rke2/ -rf
sudo ln -s /mnt/rke2 rke2

echo "Updating Ubuntu..."
sudo timedatectl set-timezone America/Toronto
sudo apt-get update -y
sudo sed -i "s/#\$nrconf{kernelhints} = -1;/\$nrconf{kernelhints} = -1;/g" /etc/needrestart/needrestart.conf
sudo sed -i "s/#\$nrconf{restart} = 'i';/\$nrconf{restart} = 'l';/g" /etc/needrestart/needrestart.conf
sudo apt-get dist-upgrade --assume-yes
sudo apt-get autoremove -y
sudo apt-get autoclean -y

sudo reboot

# change DNS, update, and reboot
# sudo sed -i 's/\[192\.168\.211\.4, 192\.168\.211\.3\]/\[192\.168\.211\.2\]/g' /etc/netplan/50-cloud-init.yaml
# sudo netplan --debug apply
# sudo apt-get update -y
# sudo apt-get dist-upgrade --assume-yes
# sudo apt-get autoremove -y
# sudo reboot
