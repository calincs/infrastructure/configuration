# Integrating GitLab with Kubernetes

## Agent-based cluster integration

Since v15, GitLab requires users to integrate k8s cluster using the [GitLab Agent](https://docs.gitlab.com/ee/user/clusters/agent/index.html). LINCS also uses agents now to connect to k8s. Details on the LINCS strategy and upgrade process can be found [here](https://gitlab.com/calincs/infrastructure/configuration/-/issues/72).

## GitLab Runner

You can install [GitLab runner](https://docs.gitlab.com/runner/install/) into the `gitlab-runner` namespace using the default Helm chart with these custom values:

```yaml
checkInterval: 3
concurrent: 25
gitlabUrl: https://gitlab.com/
runnerToken: "your_registration_token"
rbac:
  create: true
  clusterWideAccess: true
securityContext:
  allowPrivilegeEscalation: true
  privileged: true
runners:
  name: "k8s-runner"
  config: |
    [[runners]]
      environment = ["GIT_SSL_NO_VERIFY=true", "DOCKER_HOST=tcp://dind.gitlab-runner.svc.cluster.local:2375"]
      executor = "docker"
      [runners.docker]
        tls_verify = false
        image = "ubuntu:24.04"
        privileged = true
        disable_entrypoint_overwrite = false
        oom_kill_disable = false
        disable_cache = false
        volumes = ["/cache"]
        shm_size = 0
        network_mode = "bridge"      
      # [runners.kubernetes]
      #   namespace = "{{.Release.Namespace}}"
      #   image = "ubuntu:24.04"
      #   privileged = true
      #   [[runners.kubernetes.volumes.empty_dir]]
      #     name = "docker-certs"
      #     mount_path = "/certs/client"
      #     medium = "Memory"
      #   [[runners.kubernetes.volumes.empty_dir]]
      #     name = "docker-share"
      #     mount_path = "/var/run"
      #     medium = "Memory"
```

Also create a Docker-in-Docker deployment that shares the docker socket.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: runner-dind
  namespace: gitlab-runner
spec:
  replicas: 1
  selector:
    matchLabels:
      app: runner-dind
  template:
    metadata:
      labels:
        app: runner-dind
    spec:
      containers:
      - name: runner-dind
        image: docker:dind
        securityContext:
          privileged: true
        env:
          - name: DOCKER_TLS_CERTDIR
            value: ""
        ports:
          - containerPort: 2375
            name: docker
        command: ["/bin/sh", "-c"]
        args:
          - |
            crond
            dockerd-entrypoint.sh
        volumeMounts:
        # - name: docker-share
        #   mountPath: /var/run
        - name: cron-config
          mountPath: /etc/cron.d/runner-dind
          subPath: runner-dind
      volumes:
      # - name: docker-share
      #   emptyDir: {}
      - name: cron-config
        configMap:
          name: cron-config
      # affinity:
      #   podAffinity:
      #     requiredDuringSchedulingIgnoredDuringExecution:
      #     - labelSelector:
      #         matchLabels:
      #           release: k8s-runner
      #       topologyKey: kubernetes.io/hostname
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cron-config
  namespace: gitlab-runner
data:
  runner-dind: |
    # clear build images older than 5 days
    0 1 * * * docker volume prune --filter all=1 -f
    0 2 * * * docker system prune -af --filter "until=$((24*5*1))h"
---
apiVersion: v1
kind: Service
metadata:
  name: dind
  namespace: gitlab-runner
spec:
  selector:
    app: runner-dind
  ports:
    - protocol: TCP
      port: 2375
      targetPort: 2375
```

Alternatively, a Linux runner can be installed that can uses the host's docker cache. You will need to install Docker. You can follow our guide [here](docs/node_maintenance.md#install-docker).

Optionally, if you have plenty of RAM, improve performance even more by creating a ramdisk at `/ramdisk`.

```bash
sudo mkdir /ramdisk
sudo chmod 777 /ramdisk
sudo mount -t tmpfs -o size=40G ramdisk /ramdisk
```

Make the ramdisk mount permanent by copying the /etc/mtab line for this mount to /etc/fstab. Then install the runner by following the instructions [here](https://docs.gitlab.com/runner/install/linux-repository.html).

Update config.toml:

```toml
concurrent = 25
check_interval = 3

[session_server]
  session_timeout = 10000

[[runners]]
  environment = ["GIT_SSL_NO_VERIFY=true"]
  name = "lincs runner"
  url = "https://gitlab.com/"
  id = <the_id>
  token = "<the_token>"
  executor = "docker"
  [runners.custom_build_dir]
    enabled = true
  [runners.cache]
    Type = "s3"
    Path = "shell-runner"
    Shared = false
    [runners.cache.s3]
      ServerAddress = "aux.lincsproject.ca"
      AccessKey = "runner"
      SecretKey = "iortjk45sd"
      BucketName = "runners-cache"
      BucketLocation = "us-east-1"
      Insecure = false
  [runners.docker]
    tls_verify = false
    image = "ruby:bookworm"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/ramdisk/cache:/cache:rw", "/ramdisk/builds:/builds:rw"]
    shm_size = 0
    wait_for_services_timeout = 1
```

Also create a cron job to remove unused images and volumes for Docker every now and then, or you will run out of disk space eventually.

```bash
sudo docker system df
crontab -e
```

```bash
# clear build images older than a week
0 1 * * * docker volume prune --filter all=1
0 2 * * * docker system prune -af --filter "until=$((24*5*1))h"
```

## Starboard

GitLab uses [Starboard](https://aquasecurity.github.io/starboard) to scan cluster images in the `test` phase of the CI pipeline. To enable these scans, simply install the `Starboard Operator` into the `starboard` namespace for the review environment cluster using the official Helm chart in the `aqua` repo: `https://aquasecurity.github.io/helm-charts/`. Change only the following in the `values.yaml`:

```yaml
trivy:
  ignoreUnfixed: `true`
```

## Prometheus community edition

Prometheus provides metrics of deployed projects in GitLab. Here follows the instructions to install a [cluster integrated](https://docs.gitlab.com/ee/user/clusters/integrations.html#prometheus-cluster-integration) prometheus server for a cluster. However, it is deprecated and no longer supported.

Add the prometheus community helm chart repository (`https://prometheus-community.github.io/helm-charts`) and choose to install the `prometheus` chart. Make sure the chart is installed into the `gitlab-managed-apps` namespace. The following values need to be changed in `values.yaml` for the `14.6.0` version of the chart:

```yaml
alertmanager:
  enabled: false
nodeExporter:
  enabled: false
pushgateway:
  enabled: false
server:
  name: prometheus-server
  retention: 7d
```

Make sure the service is called `prometheus-prometheus-server`. Once the Prometheus installation is done, you can enable cluster integration on the _Integration_ tab of the cluster page in GitLab.
