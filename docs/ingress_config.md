# Ingress configuration for a new cluster

## Install Cert-Manager

If you left the defaults for the cluster creation then the Nginx Ingress Controller will be installed and configured. All we have to do is to set up TLS termination by configuring Cert-Manager. This involves installing the *Jetstack* Helm chart, running the CRD script, and creating a ClusterIssuer.

### Install the Helm chart

Ensure that you followed the steps at the bottom of the [install_rancher.md](docs/install_rancher.md) document to add the Jetstack Helm repo `https://charts.jetstack.io` to Rancher's catalog. Open the *System* project for the cluster and navigate to the *Apps* tab and click the *Launch* button. Search for cert-manager and choose the Jetstack chart.

### Add Cert-Manager CRDs

On the Helm chart installation form, make sure that the namespace is *cert-manager*. We also need to tell the chart to use a custom *ClusterIssuer* that we will create later. This is done by adding entries to the `values.yaml` of the chart:

```yaml
ingressShim:
  defaultIssuerName: letsencrypt
  defaultIssuerKind: ClusterIssuer
  defaultIssuerGroup: cert-manager.io
installCRDs: true
```

Click the *Launch* button to install Cert-Manager.

### Create the ClusterIssuer

Now we'll create our *issuer* to serve certificates for our cluster. Create a *ClusterIssuer* by running the following script:

```bash
cat <<EOF | kubectl apply -f -
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
  namespace: cert-manager
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: pbotha@uoguelph.ca
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx
EOF
```

### Testing Ingress

Install the following k8s template from your `kubectl` shell to see if ingress is working properly:

```bash
cat <<EOF | kubectl apply -f -
---
apiVersion: v1
kind: Namespace
metadata:
  name: test
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-test
  namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
---
kind: Service
apiVersion: v1
metadata:
  name: nginx-test
  namespace: test
spec:
  selector:
    app: nginx
  type: ClusterIP
  ports:
  - name: http
    port: 80
    targetPort: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  name: nginx-test
  namespace: test
spec:
  rules:
  - host: test.dev.lincsproject.ca
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx-test
            port:
              number: 80
  tls:
  - hosts:
    - test.dev.lincsproject.ca
    secretName: tls-secret
EOF
```

The script will install an Nginx deployment, a ClusterIP service, and an Ingress. You can monitor these deployments by navigating to *Resources*->*Workloads* under your default project.

## LINCS single ingress controller configuration

The LINCS production and stage clusters are set up with one small master node that hosts the *etcd* and *control pane* roles, and several other big *worker* nodes. The master node is configured with a public IP address and receives all ingress requests for the domain. But without a *worker* role, the master node is tainted to not run workloads and the Nginx Ingress Controller won't be scheduled to run on the master node.

So, to run the Ingress controller only on the master node, we need to modify the Nginx Ingress Controller deployment slightly. There are two ways to achieve this that are discussed below.

### Edit the cluster configuration

Since the LINCS Nginx Ingress is installed with the cluster creation, we need to edit the cluster by clicking on the *Cluster* tab after selecting the cluster below the *Global* menu. Then click on the three dots menu on the right and click *Edit*. Click the *Edit as YAML* button on the right to edit the configuration.

Under the `ingress` label you need to specify the node selector to force nginx to run on the master node:

```yaml
  ingress:
    node_selector:
      kubernetes.io/hostname: prod-master
    provider: nginx
```

When you hit save, Rancher will apply the settings and re-deploy Nginx. I have found this method to be a hit-and-miss though. The alternative is to directly go and edit the deployments as described below.

### Edit the Nginx Ingress Controller deployment

Open the *System* project for the cluster and navigate to *Resources*->*Workloads*. Scroll down to the *ingress-nginx* namespace and then click the menu icon to the right of the *nginx-ingress-controller* and select *View/Edit YAML*. Then add the nodeSelector and tolerations to the spec as shown here:

```yaml
spec:
  template:
    spec:
      nodeSelector:
        kubernetes.io/hostname: prod-master
      tolerations:
      - effect: NoExecute
        operator: Exists
      - effect: NoSchedule
        operator: Exists
```

After you hit the save button, you can click on the *nginx-ingress_controller* to confirm that it is deployed to the correct node.

*I have found that one needs to remove the node-affinity in the deployment spec, otherwise a pod won't be scheduled on a node.*
