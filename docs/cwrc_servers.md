# Summary of CWRC servers

A list fo CWRC servers is kept here for quick reference in the context of the LINCS project. The official and updated CWRC server list is located here: https://sites.google.com/a/ualberta.ca/cwrc-technical/home/cwrc-equipment/servers/server-overview (requires access rights)

## Virtual Machines

### Arbutus

#### 206.12.92.249

sparql.cwrc.ca\
Nginx with the cwrc sparql website

#### 206.12.88.125

monitor.services.cwrc.ca\
Contains Ansible playbooks (public keys, Nagios nrpe, docker, dnf auto updates, dev tools). Readme on the server

#### 206.12.88.111

cwrc.ca\
CWRC production Islandora stack

#### 206.12.90.56

XML Database server for production, staging, test, and dev CWRC instances

#### 206.12.88.124

test-01.cwrc.ca staging-01.cwrc.ca ci-01.cwrc.ca\
Continuous integration server. Git web hook endpoints.

#### 206.12.88.128

dev-01.cwrc.ca dev-02.cwrc.ca dev-02.cwrc.ca\
Development VM. Islandora stack with triple store.

#### 206.12.88.130

validator.services.cwrc.ca\
Validator services for CWRC-writer

#### 206.12.90.65

nerve.services.cwrc.ca

#### 206.12.91.81

orlando.cwrc.ca\
Orlando Delivery

#### 206.12.91.33

dev-01.orlando.cwrc.ca\
Basic setup for Orlando 2.0 dev work (docker/docker-compose & git)

#### 206.12.92.57

cwrc-writer.cwrc.ca dev-cwrc-writer.cwrc.ca\
Containerized version of cwrc-writer for dev and prod
