# LINCS Keycloak, RabbitMQ, and Keda services

[Keycloak](https://www.keycloak.org/) is an open source identity and access management system for modern applications and services. It is an ideal solution to provide LINCS with an SSO experience across the many products and services in the LINCS tool chain. It is currently planned to utilise Keycloak for NSSI and Researchspace authentication but Keycloak's flexibility allows it to be adapted to other future services too.

[RabbitMQ](https://www.rabbitmq.com/) is the most widely deployed open source message broker today. It provides NSSI services the queues to manage workflow on the LINCS infrastructure.

Both these products are globally deployed for LINCS instead of with a specific project because of the central control and simpler integration we can achieve when all LINCS tools use the same services. They both also have well-supported Bitnami charts that makes deployment to our Kubernetes cluster very simple.

[Keda](https://keda.sh/) is an autoscaling solution that NSSI uses to manage scaling of containers.

## Keda

The official [Helm chart](https://kedacore.github.io/charts) needs to be installed and then the required RBAC config for the NSSI service accounts needs to be created. Replace `{{.Release.Namespace}}` with the NSSI namespace for each environment (e.g. `nssi-16893933-staging`):

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: gitlab-keda-role
  namespace: {{.Release.Namespace}}
rules:
- apiGroups:
  - keda.sh
  resources:
  - scaledobjects
  verbs:
  - create
  - get
  - delete
  - list
  - patch
  - update
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: gitlab-keda-rolebinding
  namespace: {{.Release.Namespace}}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: gitlab-keda-role
subjects:
  - kind: ServiceAccount
    name: {{.Release.Namespace}}-service-account
    namespace: {{.Release.Namespace}}
---
apiVersion: scheduling.k8s.io/v1
kind: PriorityClass
metadata:
  name: nssi-normal-priority
value: 1000000
globalDefault: false
description: "This is the default priority class for NSSI services."
```

## Keycloak configuration

[Bitnami chart](https://github.com/bitnami/charts/tree/master/bitnami/keycloak)

The following Helm variables were set for the LINCS installation:

```yaml
auth:
  adminPassword: <thePassword>
command:
  - /bin/bash
  - '-c'
  - >-
    curl -LJ
    https://github.com/eosc-kc/keycloak-orcid/releases/download/1.0.0/keycloak-orcid-1.0.0.jar
    -o /opt/bitnami/keycloak/providers/keycloak-orcid-1.0.0.jar;
    /opt/bitnami/scripts/keycloak/entrypoint.sh /opt/bitnami/scripts/keycloak/run.sh
extraStartupArgs: '--features=token-exchange'
ingress:
  annotations:
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "128k"
  enabled: true
  hostname: keycloak.dev.lincsproject.ca
  ingressClassName: 'nginx'
  path: /
  pathType: ImplementationSpecific
  servicePort: http
  tls: true
metrics:
  enabled: true
postgresql:
  auth:
    password: <thePassword>
    postgresPassword: <thePassword>
# if you have an existing Postgres db, specify the same major version
  image:
    tag: 14.7.0
proxyHeaders: forwarded
service:
  type: ClusterIP
```

The administrator username is `user` and the password is stored in a Kubernetes secret in the Keycloak namespace. Log into Keycloak and create a realm called `lincs`. Then run the ci pipeline for the [Keycloak Configuration](https://gitlab.com/calincs/infrastructure/keycloak-config) project.

[This](https://www.keycloak.org/getting-started/getting-started-kube) guide will help to get started with Keycloak configuration while [this](https://www.keycloak.org/docs/latest/securing_apps/index.html#other-openid-connect-libraries) guide will show how to use *OpenID Connect*.

The [NSSI](https://gitlab.com/calincs/conversion/NSSI/-/wikis/Keycloak-Guide) project has further instructions on managing authentication with Keycloak in the NSSI environment.

### Upgrading Keycloak's Postgres database

The basic process involves exporting the database with the old version, and then importing the database with the new version of Postgres.

Start a shell into the existing PG pod and run:

```bash
cd /bitnami/postgresql
pg_dump -U bn_keycloak -Fc bitnami_keycloak > keycloak.dump
```

This will create a dump file on the persistent volume. Next, we want to start up a pod that mounts this volume so that we can delete the old Postgres database files. The easiest is to edit the current Postgres StatefulSet and add a `command` property for the Postgres container so that it doesn't start Postgres automatically.

```yaml
    command: ['/bin/sh', '-c', 'sleep 50000']
```

Shell into the pod once it starts up and run:

```bash
rm /bitnami/postgresql/data/* -rf
```

Now we can edit the yaml again and remove the command property. Also, change the image tag of the Postgres image to the new version. Once the new pod starts up, it should create a blank database with the new version.

Now we can start a shell into this pod and restore the old database:

```bash
pg_restore -U bn_keycloak -d bitnami_keycloak keycloak.dump
```

## RabbitMQ configuration

[Bitnami chart](https://github.com/bitnami/charts/tree/master/bitnami/rabbitmq)

The following yaml was used to configure the RabbitMQ Helm chart (v11.16.2) for LINCS:

```yaml
auth:
  password: WC86M0y3Xn3KnalI
metrics:
  enabled: true
  prometheusRule:
    enabled: true
  serviceMonitor:
    enabled: true
loadDefinition:
  enabled: true
  existingSecret: load-definition
  file: /app/load_definition.json
ingress:
  annotations:
    kubernetes.io/tls-acme: 'true'
  enabled: true
  hostname: rmq.dev.lincsproject.ca
  pathType: ImplementationSpecific
  tls: true
extraConfiguration: |
  load_definitions = /app/load_definition.json
extraSecrets:
  load-definition:
    load_definition.json: |
      {
        "users": [
          {
            "name": "nssi_services",
            "password": "unuK8y-5vCq+3W534TsA3m"
            "tags": ""
          },
          {
            "name": "rmq_admin",
            "password": "WC86M0y3Xn3KnalI"
            "tags": "administrator"
          },
          {
            "name": "recon_service",
            "password": "unuK8y-5vCq+3W534TsA3m",
            "tags": ""
          },
          {
            "name": "keda",
            "password": "vpthIoJD1rWOw7ri",
            "tags": ""
          }
        ],
        "vhosts": [
          {
            "name": "/"
          },
          {
            "name": "nssi"
          }
        ],
        "permissions": [
          {
            "user": "rmq_admin",
            "vhost": "/",
            "configure": ".*",
            "write": ".*",
            "read": ".*"
          },
          {
            "user": "nssi_services",
            "vhost": "nssi",
            "configure": ".*",
            "write": ".*",
            "read": ".*"
          },
          {
            "user": "recon_service",
            "vhost": "nssi",
            "configure": ".*",
            "write": ".*",
            "read": ".*"
          },
          {
            "user": "rmq_admin",
            "vhost": "nssi",
            "configure": ".*",
            "write": ".*",
            "read": ".*"
          },
          {
            "user": "keda",
            "vhost": "nssi",
            "configure": ".*",
            "write": ".*",
            "read": ".*"
          }
        ],
        "topic_permissions": [],
        "parameters": [],
        "policies": [],
        "queues": [
          {
            "name": "deadLetterQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {}
          },
          {
            "name": "annotationQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "stanfordnerQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "linkingQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "extractionQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "progressQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "reconciliationQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          },
          {
            "name": "resultQueue",
            "vhost": "nssi",
            "durable": true,
            "auto_delete": false,
            "arguments": {
              "x-dead-letter-exchange": "appMessages.dlx"
            }
          }
        ],
        "exchanges": [
          {
            "name": "appMessages.dlx",
            "vhost": "nssi",
            "type": "fanout",
            "durable": true,
            "auto_delete": false,
            "internal": false,
            "arguments": {}
          },
          {
            "name": "progressExchange",
            "vhost": "nssi",
            "type": "direct",
            "durable": true,
            "auto_delete": false,
            "internal": false,
            "arguments": {}
          },
          {
            "name": "appMessagesExchange",
            "vhost": "nssi",
            "type": "direct",
            "durable": true,
            "auto_delete": false,
            "internal": false,
            "arguments": {}
          }
        ],
        "bindings": [
          {
            "source": "appMessages.dlx",
            "vhost": "nssi",
            "destination": "deadLetterQueue",
            "destination_type": "queue",
            "routing_key": "",
            "arguments": {}
          },
          {
            "source": "appMessagesExchange",
            "vhost": "nssi",
            "destination": "annotationQueue",
            "destination_type": "queue",
            "routing_key": "annotationQueue",
            "arguments": {}
          },
          {
            "source": "appMessagesExchange",
            "vhost": "nssi",
            "destination": "extractionQueue",
            "destination_type": "queue",
            "routing_key": "extractionQueue",
            "arguments": {}
          },
          {
            "source": "appMessagesExchange",
            "vhost": "nssi",
            "destination": "linkingQueue",
            "destination_type": "queue",
            "routing_key": "linkingQueue",
            "arguments": {}
          },
          {
            "source": "appMessagesExchange",
            "vhost": "nssi",
            "destination": "reconciliationQueue",
            "destination_type": "queue",
            "routing_key": "reconciliationQueue",
            "arguments": {}
          },
          {
            "source": "appMessagesExchange",
            "vhost": "nssi",
            "destination": "stanfordnerQueue",
            "destination_type": "queue",
            "routing_key": "stanfordnerQueue",
            "arguments": {}
          },
          {
            "source": "progressExchange",
            "vhost": "nssi",
            "destination": "progressQueue",
            "destination_type": "queue",
            "routing_key": "progressQueue",
            "arguments": {}
          }
        ]
      }
```
