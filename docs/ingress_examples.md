# Ingress Use Cases

Reference: [https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/)

## Forward to an external service

Let's say we want to forward all traffic at endpoint `huviz-classic.lincsproject.ca` to an external service located in another cluster: `20011932-review-classic-cl4okp.dev.lincsproject.ca` a la an API gateway.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    nginx.ingress.kubernetes.io/upstream-vhost: "20011932-review-classic-cl4okp.dev.lincsproject.ca"
  name: huviz-classic
  namespace: huviz-classic
spec:
  rules:
  - host: huviz-classic.lincsproject.ca
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: huviz-classic
            port:
              number: 443
  tls:
  - hosts:
    - huviz-classic.lincsproject.ca
    secretName: tls-secret
---
apiVersion: v1
kind: Service
metadata:
  name: huviz-classic
  namespace: huviz-classic
spec:
  type: ExternalName
  externalName: 20011932-review-classic-cl4okp.dev.lincsproject.ca
```

## Forward to local service

This ingress will redirect all traffic from `www.lincsproject.ca` to `lincsproject.ca`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: www.lincsproject.ca
  namespace: lincsaurus-32806093-production
  annotations:
    nginx.ingress.kubernetes.io/server-snippet: |
      return 301 $scheme://lincsproject.ca$request_uri;
spec:
  ingressClassName: nginx
  tls:
  - hosts:
    - www.lincsproject.ca
    secretName: www-tls
  rules:
  - host: www.lincsproject.ca
```

or do this if server snippets are disabled:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: www.lincsproject.ca
  namespace: lincsaurus-32806093-production
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: 'true'
    nginx.ingress.kubernetes.io/permanent-redirect: https://lincsproject.ca
spec:
  ingressClassName: nginx
  tls:
    - hosts:
        - www.lincsproject.ca
      secretName: www-tls
  rules:
    - host: www.lincsproject.ca
```

## Change URL and forward to local service

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/tls-acme: 'true'
    nginx.ingress.kubernetes.io/rewrite-target: >-
      https://rs-review.lincsproject.ca/resource/?uri=http%3A%2F%2Fid.lincsproject.ca%2F$1
    nginx.ingress.kubernetes.io/server-snippet: >
      rewrite (\/|$)(.*) https://rs-review.lincsproject.ca/resource?uri=http%3A%2F%2Fid.lincsproject.ca%2F$2 last;
      return 301 https://rs-review.lincsproject.ca/resource?uri=http%3A%2F%2Fid.lincsproject.ca%2F$2%23$3;
  name: lincs-id-ingress
  namespace: lincs-rs-custom-app-23837748-review
spec:
  ingressClassName: nginx
  rules:
    - host: id.lincsproject.ca
      http:
        paths:
          - backend:
              service:
                name: platform
                port:
                  number: 8080
            path: /(.*)
            pathType: Prefix
  tls:
    - hosts:
        - id.lincsproject.ca
      secretName: lincs-id-tls
```

## Allow CORS traffic

```yaml
    nginx.ingress.kubernetes.io/enable-cors: "true"
```

## Allow bigger downloads

```yaml
    nginx.ingress.kubernetes.io/proxy-body-size: 1000m
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "300"
```

## Fix some 502 responses

```yaml
    nginx.ingress.kubernetes.io/proxy-buffer-size: "128k"
    nginx.ingress.kubernetes.io/proxy-buffering: "on"
    nginx.ingress.kubernetes.io/proxy-buffers-number: "4"
```

## Other useful annotations

```yaml
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS" # (HTTP, HTTPS, GRPC, GRPCS, AJP or FCGI)
    nginx.ingress.kubernetes.io/default-backend: service_name
```

## Nginx server snippets

```yaml
    nginx.ingress.kubernetes.io/server-snippets: |
      location /xmpp-websocket {
          proxy_pass http://localhost;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
      }
      location /colibri-ws {
          proxy_pass http://localhost;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
      }
```
