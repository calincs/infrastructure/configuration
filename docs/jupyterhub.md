# JupyterHub

This setup installs a JupyterHub server that is integrated with GitLab for authentication. Jupyterhub is not supported anymore and the official GitLab [instructions](https://docs.gitlab.com/ee/user/project/clusters/runbooks/) were based on an old version of `zero-too-jupyterhub` helm charts (v0.9.0).

## Create an OAuth Application for JupyterHub

Follow the instructions [here](https://docs.gitlab.com/ee/integration/oauth_provider.html#gitlab-as-oauth2-authentication-service-provider).

## Install JupyterHub Helm chart

We're using the `zero-to-jupyterhub` Helm chart with modified instructions originally from [here](https://zero-to-jupyterhub.readthedocs.io/en/latest/jupyterhub/installation.html) with OAuth customization [here](https://oauthenticator.readthedocs.io/en/stable/getting-started.html).

First step is to add the Helm repo: [https://jupyterhub.github.io/helm-chart/](https://jupyterhub.github.io/helm-chart/).

Then, install the Helm chart (tested with v1.2.0) with the following parameters changed in `config.yaml`:

```yaml
hub:
  config:
    Authenticator:
      admin_users:
        - zacanbot
      enable_auth_state: true
    GitLabOAuthenticator:
      client_id: 12b6d72d9511bb8e4a0cf794c9509abe3086a5ece4992fd83413af94b2c56b84
      client_secret: <get from cagroup oauth applications>
      oauth_callback_url: http://jupyter.lincsproject.ca/hub/oauth_callback
      # allowed_gitlab_project_ids: []
      allowed_gitlab_groups:
        - calincs
    JupyterHub:
      admin_access: true
      authenticator_class: gitlab
  containerSecurityContext:
    runAsUser: 0

ingress:
  annotations:
    kubernetes.io/ingress.class: "nginx"
    kubernetes.io/tls-acme: "true"
  enabled: true
  hosts:
    - jupyter.lincsproject.ca
  pathType: Prefix
  tls:
    - hosts:
      - jupyter.lincsproject.ca
      secretName: jupyter-cert

proxy:
  secretToken: "9cd66cf1fa8218f31dbae1dfe4bbca8eaaccbb52e69570a19219745dbac8b46c"
  service:
    type: ClusterIP

singleuser:
  defaultUrl: "/lab"
  image:
    name: jupyter/scipy-notebook # https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html
    tag: hub-2.2.2
  memory:
    guarantee: 2G
    limit: 20G
  initContainers:
    - command:
        - sh
        - '-c'
        - (chmod -R 0775 /data; chown -R 1000:100 /data)
      image: alpine
      name: ownership-fixer
      securityContext:
        runAsUser: 0
      volumeMounts:
        - mountPath: /data
          name: volume-{username}
  storage:
    capacity: 6Gi
```

The following helm install command can be used if the above configuration was saved in `config.yaml`:

```bash
helm upgrade --cleanup-on-fail \
  --install jupyterhub jupyterhub/jupyterhub \
  --namespace jupyter-hub \
  --create-namespace \
  --version=1.2.0 \
  --values config.yaml
```
