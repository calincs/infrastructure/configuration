# Jitsi-Meet

We used the helm chart maintained [here](https://github.com/jitsi-contrib/jitsi-helm) to install Jitsi for LINCS.

## Host port configuration

We need to open up the Jitsi ports in Openstack for the cluster's worker nodes.

## Install Jitsi from the Helm chart

NB! It is required that the nodeSelector for the JVB in the previous step points to the node that has a public IP. You have to expose the UDP port (30400) on the node where JVB pod runs to the outside world.

Add the Jitsi Helm repo: [https://jitsi-contrib.github.io/jitsi-helm/](https://jitsi-contrib.github.io/jitsi-helm/)

Install the Helm chart into the `jitsi-meet` namespace and call the app `jitsi`. Change the following parameters in values.yaml (tested with v1.33 of the chart):

```yaml
jibri:
  xmpp:
    password: <password>
jicofo:
  xmpp:
    password: <password>
jvb:
  UDPPort: 30400
  nodeSelector:
    kubernetes.io/hostname: production-node-6250af03-2fjw9
  useHostPort: true
  xmpp:
    password: <password>
publicURL: 'https://jitsi.lincsproject.ca'
tz: America/Toronto
web:
  extraEnvs:
    DESKTOP_SHARING_FRAMERATE_MIN: "15"
    DESKTOP_SHARING_FRAMERATE_MAX: "15"
  ingress:
    annotations:
      kubernetes.io/ingress.class: nginx
      kubernetes.io/tls-acme: 'true'
      nginx.ingress.kubernetes.io/proxy-read-timeout: '3600'
      nginx.ingress.kubernetes.io/proxy-send-timeout: '3600'
    enabled: true
    hosts:
      - host: jitsi.lincsproject.ca
        paths:
          - /
    tls:
      - secretName: jitsi-tls-secret
        hosts:
          - jitsi.lincsproject.ca
```

## Configuration

Jitsi can be customised by setting the `web.extraEnvs` property of the chart. A list of available configuration variables can be found in the Docker Compose file here: <https://github.com/jitsi/docker-jitsi-meet/blob/master/docker-compose.yml>

Alternatively, you can mount the following file at `/config/config.js` in the `jitsi-meet-web` container:

```javascript
// Jitsi Meet configuration.
var config = {};

if (!config.hasOwnProperty('hosts')) config.hosts = {};

config.hosts.domain = 'meet.jitsi';
config.focusUserJid = 'focus@auth.meet.jitsi';

var subdomain = "<!--# echo var="subdomain" default="" -->";
if (subdomain) {
    subdomain = subdomain.substr(0,subdomain.length-1).split('.').join('_').toLowerCase() + '.';
}
config.hosts.muc = 'muc.'+subdomain+'meet.jitsi';
config.bosh = '/http-bind';
config.websocket = 'wss://jitsi.lincsproject.ca/xmpp-websocket';


// Video configuration.
//

if (!config.hasOwnProperty('constraints')) config.constraints = {};
if (!config.constraints.hasOwnProperty('video')) config.constraints.video = {};

config.resolution = 720;
config.constraints.video.height = { ideal: 720, max: 720, min: 180 };
config.constraints.video.width = { ideal: 1280, max: 1280, min: 320};
config.disableSimulcast = false;
config.startVideoMuted = 10;
config.startWithVideoMuted = false;

if (!config.hasOwnProperty('flags')) config.flags = {};
config.flags.sourceNameSignaling = false;
config.flags.sendMultipleVideoStreams = false;


// ScreenShare Configuration.
//
config.desktopSharingFrameRate = { min: 5, max: 5 };

// Audio configuration.
//

config.enableNoAudioDetection = true;
config.enableTalkWhileMuted = false;
config.disableAP = false;

if (!config.hasOwnProperty('audioQuality')) config.audioQuality = {};
config.audioQuality.stereo = false;

config.startAudioOnly = false;
config.startAudioMuted = 10;
config.startWithAudioMuted = false;
config.startSilent = false;
config.enableOpusRed = false;
config.disableAudioLevels = false;
config.enableNoisyMicDetection = true;


// Peer-to-Peer options.
//

if (!config.hasOwnProperty('p2p')) config.p2p = {};

config.p2p.enabled = true;


// Breakout Rooms
//

config.hideAddRoomButton = false;


// Etherpad
//

// Recording.
//

// Analytics.
//

if (!config.hasOwnProperty('analytics')) config.analytics = {};

// Enables callstatsUsername to be reported as statsId and used
// by callstats as repoted remote id.
config.enableStatsID = false;


// Dial in/out services.
//


// Calendar service integration.
//

config.enableCalendarIntegration = false;

// Invitation service.
//

// Miscellaneous.
//

// Prejoin page.
if (!config.hasOwnProperty('prejoinConfig')) config.prejoinConfig = {};
config.prejoinConfig.enabled = true;

// Hides the participant name editing field in the prejoin screen.
config.prejoinConfig.hideDisplayName = false;
 
// List of buttons to hide from the extra join options dropdown on prejoin screen.
// Welcome page.
config.enableWelcomePage = true;

// Close page.
config.enableClosePage = false;

// Default language.
// Require users to always specify a display name.
config.requireDisplayName = false;


// Chrome extension banner.
// Advanced.
//

// Lipsync hack in jicofo, may not be safe.
config.enableLipSync = false;

config.enableRemb = true;
config.enableTcc = true;

// Enable IPv6 support.
config.useIPv6 = true;

// Transcriptions (subtitles and buttons can be configured in interface_config)
config.transcribingEnabled = false;

// Deployment information.
//

if (!config.hasOwnProperty('deploymentInfo')) config.deploymentInfo = {};

// Testing
//

if (!config.hasOwnProperty('testing')) config.testing = {};
if (!config.testing.hasOwnProperty('octo')) config.testing.octo = {};

config.testing.capScreenshareBitrate = 1;
config.testing.octo.probability = 0;

// Deep Linking
config.disableDeepLinking = false;

// P2P preferred codec
// enable preffered video Codec
if (!config.hasOwnProperty('videoQuality')) config.videoQuality = {};
config.videoQuality.enforcePreferredCodec = false;

if (!config.videoQuality.hasOwnProperty('maxBitratesVideo')) config.videoQuality.maxBitratesVideo = {};
// Reactions
config.disableReactions = false;

// Polls
config.disablePolls = false;

// Configure toolbar buttons
// Hides the buttons at pre-join screen
// Configure remote participant video menu
if (!config.hasOwnProperty('remoteVideoMenu')) config.remoteVideoMenu = {};
config.remoteVideoMenu.disabled = false;
config.remoteVideoMenu.disableKick = false;
config.remoteVideoMenu.disableGrantModerator = false;
config.remoteVideoMenu.disablePrivateChat = false;

// Configure e2eping
if (!config.hasOwnProperty('e2eping')) config.e2eping = {};
config.e2eping.enabled = false;
```
