# ElasticStack

## Install ElasticSearch first

Make sure the elastic repository is added to your helm charts: `https://helm.elastic.co` and that the `elastic` namespace exists, where we will install the elasticsearch chart. Make sure to use v7.x of the elastic chart (tested with v7.17.3) and name the deployment `elasticsearch`.

Edit the following values in `values.yaml`:

```yaml
# extraInitContainers:
# - name: file-permissions
#   image: alpine:3.6
#   command: ['chown', '-R', '1000:1000', '/usr/share/elasticsearch/data']
#   securityContext:
#     runAsUser: 0
#   volumeMounts:
#   - mountPath: /usr/share/elasticsearch/data
#     name: elasticsearch-master
minimumMasterNodes: 1
replicas: 1
extraEnvs:
- name: discovery.seed_hosts
  value: "[]"
secret:
  enabled: true
  password: <PASSWORD>
volumeClaimTemplate:
  resources:
    requests:
      storage: 5Gi
```

## Install Kibana

Kibana provides a frontend to the Elastic indexes and allow us much more functionality that just the GitLab log filter. It also provides some administration functions for the Elastic stack.

Choose the Kibana chart, install into the `elastic` namespace and change the following in values.yaml:

```yaml
kibanaConfig:
  kibana.yml: |
    server:
      publicBaseUrl: https://kibana.dev.lincsproject.ca
ingress:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: 'true'
  enabled: true
  hosts:
    - host: kibana.dev.lincsproject.ca
      paths:
        - path: /
  pathType: Prefix
  # not a typo, bug in the current helm chart:
  pathtype: Prefix
  tls:
    - hosts:
        - kibana.dev.lincsproject.ca
      secretName: kibana-tls-secret
```

## Secure the Kibana UI

We can implement usernames and passwords for the Kibana/Elastic stack but that will break the GitLab integration. Since we only expose the Kibana endpoint, it is simple to secure access on the ingress. Start with a shell with *kubectl* access to the cluster.

Create a file called `auth` that contains the username and password and use that to create a secret:

```bash
USER=leaf; PASSWORD=<our_password>; echo "${USER}:$(openssl passwd -stdin -apr1 <<< ${PASSWORD})" >> auth
kubectl -n elastic create secret generic basic-auth --from-file=auth
```

Then we can edit the Kibana ingress and add three annotations for `nginx.ingress.kubernetes.io/auth-*` so that the ingress looks something like this:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required'
.
.
.
```

Hit save and open the UI - it should ask for a username and password.

## Filebeat

When installing the official Filebeat helm chart, edit `filebeatConfig` in the values.yaml and set the `output.elasticsearch` value to point to the correct elasticsearch service for `deployment:`. Replace the entire filebeatConfig under `daemonset:` with the definition below and also adjust the resources as below:

```yaml
  filebeatConfig:
    filebeat.yml: |
      setup.ilm.enabled: auto
      setup.ilm.rollover_alias: "filebeat"
      setup.ilm.pattern: "{now/d}-000001"
      setup.template.name: 'filebeat'
      setup.template.pattern: 'filebeat-*'
      output.file.enabled: false
      output.elasticsearch:
        host: '${NODE_NAME}'
        hosts: '${ELASTICSEARCH_HOSTS:elasticsearch-master:9200}'
        index: "filebeat-%{[agent.version]}-%{+yyyy.MM.dd}"
        ilm.enabled: true
      filebeat.inputs:
      - type: container
        paths:
          - '/var/lib/docker/containers/*/*.log'
        json.keys_under_root: true
        json.ignore_decoding_error: true
        processors:
          - add_id:
              target_field: tie_breaker_id
          - add_kubernetes_metadata: ~
          - drop_event.when.or:
            - equals.kubernetes.namespace: "gitlab-managed-apps"
            - equals.kubernetes.namespace: "longhorn-system"
            - equals.kubernetes.namespace: "cattle-monitoring-system"
            - not.has_fields: ['kubernetes.namespace']
  resources:
    limits:
      cpu: 1000m
      memory: 400Mi
    requests:
      cpu: 200m
      memory: 200Mi
```

After Filebeat is deployed, edit the filebeat DaemonSet yaml and change the `varlibdockercontainers` volumes.hostPath.path from `/var/lib/docker/containers` to `/mnt/docker/containers`.

You can now go ahead and enable ElasticStack integration for your Gitlab cluster.

### *RKE2 Nodes note*

RKE2 does not use docker and the container logs are in a different place. Use this config:

```yaml
  filebeatConfig:
    filebeat.yml: |
      filebeat.inputs:
      - type: container
        paths:
          - /var/log/containers/*.log
        processors:
        - add_kubernetes_metadata:
            host: ${NODE_NAME}
            matchers:
            - logs_path:
                logs_path: "/var/log/containers/"
        - drop_event.when.or:
          - equals.kubernetes.namespace: "gitlab-managed-apps"
          - equals.kubernetes.namespace: "longhorn-system"
          - equals.kubernetes.namespace: "calico-system"
          - equals.kubernetes.namespace: "kube-system"
          - equals.kubernetes.namespace: "cattle-fleet-system"
          - equals.kubernetes.namespace: "tigera-operator"
          - equals.kubernetes.namespace: "cattle-monitoring-system"
          - not.has_fields: ['kubernetes.namespace']
      output.elasticsearch:
        host: '${NODE_NAME}'
        hosts: '${ELASTICSEARCH_HOSTS:elasticsearch-master:9200}'
        index: "filebeat-%{[agent.version]}-%{+yyyy.MM.dd}"
        ilm.enabled: true
      output.file.enabled: false
      setup.ilm.enabled: auto
      setup.ilm.rollover_alias: "filebeat"
      setup.ilm.pattern: "{now/d}-000001"
      setup.template.name: 'filebeat'
      setup.template.pattern: 'filebeat-*'
```

## Life cycle policy

Now we need to set up the index lifecycle management. Browse to the Kibana endpoint and use the menu on the left to open the `Dev Tools`. Run the following snippet to update the filebeat ilm policy:

```json
PUT _ilm/policy/filebeat
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_primary_shard_size": "2gb",
            "max_age": "10d"
          }
        }
      },
      "delete": {
        "min_age": "30d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}
```

## Export k8s events to Elastic

We can export events with the [Kubernetes event exporter](https://github.com/opsgenie/kubernetes-event-exporter). This will allow events to be persisted in Elastic instead of being deleted and then unavailable after the default one hour. Install the Bitnami chart for `kubernetes-event-exporter` in the `elastic` namespace and change the following in values.yaml:

```yaml
config:
  logFormat: pretty
  logLevel: debug
  receivers:
    - name: dump
      elasticsearch:
        hosts:
        - http://elasticsearch-master:9200
        index: kube-events
        indexFormat: "kube-events-{2006-01}"
  route:
    routes:
      - match:
          - receiver: dump
```
