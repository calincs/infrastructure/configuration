# Kubernetes storage guide

Instead of provisioning Cinder volumes directly via the Cinder driver for Kubernetes, we are mounting the volumes on worker nodes and provisioning storage through the Longhorn provisioner for Kubernetes. Here are some of the reasons for adding yet another layer of abstraction to the storage:

* Compute Canada does not guarantee the Cinder API to be available when the hosts are running. During upgrades and maintenance, the Cinder API could be taken offline while the VMs could still be running. This will prevent Kubernetes from connecting workloads to their storage and create all sorts of problems.
* Backing up entire Cinder volumes isn't possible in our CC environment. CC only supports file-based backups, which would require an administrator to log into running containers to access the file system and backup the files on every individual deployment.
* A directly-mounted Cinder volume on Arbutus can be horrendously slow - orders of magnitude less than local storage.
* As a Compute Canada client, we don't have access to the raw Ceph storage backend as CC can't provision that storage in a multi-tenant environment.

By using the Longhorn storage provisioner on stripe sets of mounted volumes in hosts, we are addressing most of these issues:

* All Cinder volumes are mounted directly in the VM hosts. Even during maintenance when the Cinder API goes offline, the Cinder volumes would stay mounted and be accessible.
* Longhorn provides the ability to detach entire volumes and back them up to S3 storage.
* Possibly, due to bandwidth policies of the various layers of the NAS setup of Arbutus, it actually helps to create stripe sets of Cinder volumes to increase storage bandwidth.
* Longhorn implements some queueing and caching that reduces latency and increases bandwidth compared to direct Cinder mounts. An average of 2x to 3x performance improvement was measured on the LINCS infrastructure.
* Kubernetes volume expansion works properly with the Longhorn provisioner - not so with the Cinder provisioner.

Outside of k8s, there are also 8 5TB volumes mounted on the [backup server](https://aux.lincsproject.ca) for the LINCS S3 storage solution. These volumes are not striped as Minio does its own striping across volumes. For more details around the Minio storage solution, see the [minio_s3_storage.md](docs/minio_s3_storage.md) document.

## Longhorn management

Each k8s cluster is set up with a Longhorn driver that serves as the default storage provisioner for that cluster. Management of persistent volume claims and the volumes themselves can be done as usual in the Rancher front-end. However, there is an additional storage front-end installed for each cluster that makes the management of backups and maintenance much easier.

## Creating a new node with attached storage

1. Create a new worker node in Rancher.
2. Create 8 2TB volumes in the OpenStack Horizon web UI.
3. Attach those volumes to the new host from the Volumes tab in Horizon.
4. Add a floating IP to the new host, copy SSH keys from Rancher, and SSH into the new host by following the instructions in the [node_maintenance.md](docs/node_maintenance.md) file.
5. Run the following commands to create the volume:

```bash
sudo apt-get update
sudo apt-get dist-upgrade
lsblk
sudo pvcreate -v /dev/vd[c-f]
sudo vgcreate -v vg01_data /dev/vd[c-f]
sudo lvcreate -l 100%FREE -i 4 -I 128k -n lv_data vg01_data -v
sudo mkfs.ext4 /dev/mapper/vg01_data-lv_data
sudo mkdir /longhorn
sudo mount /dev/vg01_data/lv_data /longhorn
```

That last command would have created the correct fstab mount configuration for this volume. Copy the last line in mtab (`cat /etc/mtab` and then copy with the cursor) and then add it to the bottom of fstab after running `sudo nano /etc/fstab`. The above instructions resulted in this line:

```bash
/dev/mapper/vg01_data-lv_data /longhorn ext4 rw,relatime,attr2,inode64,logbufs=8,logbsize=128k,sunit=256,swidth=1024,noquota 0 0
```

Reboot the machine and run `df -h` to check that the volume is correctly mounted.

I had to add a delayed call to mount in rc.local to successfully mount on some nodes before:

```bash
#!/bin/sh
# notes: a sleep at least 10s is required for the mount below to work on Compute Canada's Arbutus cloud
sleep 10
printf "Trying to manually mount fstab volumes..."
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
  mount -a # mount all drives in /etc/fstab
  printf "... volumes mounted."
fi
exit 0
```

Remember to `sudo chmod +x /etc/rc.local` if you created a new file.

### Blacklist Longhorn devices for Mulitpath

See this issue: [https://gitlab.com/calincs/infrastructure/configuration/-/issues/41](https://gitlab.com/calincs/infrastructure/configuration/-/issues/41)

Edit the multipath config file: `sudo nano /etc/multipath.conf`

Add these lines:

```bash
blacklist {
    devnode "^sd[a-z0-9]+"
}
```

Then restart multipath: `sudo systemctl restart multipathd.service`

## Benchmarking storage

We used a very simple `dd` script to benchmark storage performance. We wanted to simulate typical database IO and not copy throughput. This script tests the local disk mounted in `/mnt` as well as the Cinder volume mounted in `/longhorn`

```bash
sudo dd if=/dev/zero of=/mnt/test.img bs=4k count=200 oflag=dsync
sudo dd if=/dev/zero of=/longhorn/test.img bs=4k count=200 oflag=dsync
sudo rm /mnt/test.img /longhorn/test.img
```

## Adding Longhorn to a cluster

Longhorn can be added by using the Longhorn helm chart that comes with the Rancher Catalogue. It will create the ingress to the Longhorn web UI and add the storage class to the cluster. If you need to change the storage class for any reason (e.g. to update the default number of replicas), you can delete the old storage class and add it manually using the following definition:

```yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: longhorn-fast
provisioner: driver.longhorn.io
reclaimPolicy: Delete
allowVolumeExpansion: true
volumeBindingMode: Immediate
parameters:
  numberOfReplicas: "2"
  staleReplicaTimeout: "60" # minutes
  diskSelector: fast
```

The template above specifies a diskSelector. This is useful if you have multiple disks with different performance levels that you'd want to select when creating a persistent volume claim. In the Longhorn we UI, one can associate this diskSelector label with a physical volume on a registered node. The LINCS Production environment has *longhorn* and *longhorn-fast* storage classes defined that binds to *default* and *fast* disk selectors respectively. The *fast* disk selector utilises local SSD storage on the nodes.

Depending on which Ubuntu images were used for the cluster nodes, you might need to add `iscsi` support. Longhorn manager will fail to start if it isn't available.

```bash
sudo apt-get install -y open-iscsi
```

### Minio backups

When installing Longhorn, you can configure backups by specifying these two parameters in the Helm chart:

* Backup target: s3://longhorn-prod@us-east-1/
* Secret: minio-secret

You need to then go ahead and create the secret so that Longhorn can connect to the S3 repo.

```yaml
cat <<EOF | kubectl apply -f -
---
apiVersion: v1
data:
  AWS_ACCESS_KEY_ID: bGluY3M=
  AWS_ENDPOINTS: aHR0cHM6Ly9iYWNrdXAubGluY3Nwcm9qZWN0LmNhOjQ0Mw== #https://aux.lincsproject.ca
  AWS_SECRET_ACCESS_KEY: bGluY3NfZnR3
kind: Secret
metadata:
  name: minio-secret
  namespace: longhorn-system
type: Opaque
EOF
```

### Secure the UI

There is no authentication built into the Longhorn UI and the default ingress exposes the UI to the public. We can either choose to use Rancher-Proxy when installing the Longhorn chart or we can add Basic Authentication to the ingress. The latter is implemented in the Production and Staging environments for LINCS. The following steps can be followed to add authentication to the default public ingress. Start with a shell with *kubectl* access to the cluster.

Create a file called `auth` that contains the username and password and use that to create a secret:

```bash
USER=lincs; PASSWORD=<our_password>; echo "${USER}:$(openssl passwd -stdin -apr1 <<< ${PASSWORD})" >> auth
kubectl -n longhorn-system create secret generic basic-auth --from-file=auth
```

Then we can browse to the ingress list by first opening the project in Rancher: *production* -> *System*, and then selecting *Resources* -> *Workloads* and then the *Ingress* tab. Click on the menu icon to the right of the **longhorn-ingress** and select *View/Edit YAML*. Add three annotations for `nginx.ingress.kubernetes.io/auth-*` so that the ingress looks something like this:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required'
  name: longhorn-ingress
  namespace: longhorn-system
spec:
  rules:
  - host: longhorn.lincsproject.ca
    http:
      paths:
      - backend:
          service:
            name: longhorn-frontend
            port:
              number: 80
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - longhorn.lincsproject.ca
    secretName: longhorn-cert
```

Hit save and open the UI - it should ask for a username and password.

## Cinder volume mappings

Here follows a list of the hosts responsible for provisioning storage and the volumes mounted to those hosts for the LINCS infrastructure.

* prod-w1: 4x 2TB stripe set
* prod-w2: 4x 2TB stripe set
* stage nodes: 4x 2TB stripe sets
* sandbox nodes: 2x 2TB stripe sets
* backup: 8x 5TB volumes individually mounted
