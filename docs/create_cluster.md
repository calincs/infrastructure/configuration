# Provision a cluster from Rancher

After Rancher has been [installed](docs/rancher_install.md), we'll be able to log into Rancher and add/create new clusters to be managed by Rancher.

## Getting OpenStack parameters

Before we can start creating clusters and configuring nodes, we will need to gather specific information about our OpenStack environment to enable the integration with Rancher.

* OpenStack username and password: a user that can administrate the `rpp-sbrown-lincs` project on Arbutus. `zacanbot` was used for setting up LINCS
* User domain name: `CCDB`
* Project or tenant ID: `91cef58e96444b1abc6abdf09f9a9bf7`
* Authentication URL: `https://arbutus.cloud.computecanada.ca:5000/v3`
* Router ID: `515bde77-c39c-4d6e-a24a-1ae1470876f4`
* Public Network ID: `6621bf61-6094-4b24-a9a0-f5794c3a881e`
* Private Network ID: `af61b790-80a3-432e-84c6-df56f8c99e6f`
* Private Subnet ID: `0d49df76-64ad-48d2-bea2-4fde8f232017`
* OpenStack Flavor Names: `c32-120gb-425` and `c16-180gb-576` and `c2-7.5gb-36`
* VM image name: `Ubuntu-20.04-Focal-minimal-x64-2020-12`

This information is available on the OpenStack Horizon web UI once logged into the project on Arbutus.

## Create security groups

Create a new security group called `k8s-node` with the following ports opened on the private network, so that nodes can talk to each other and to the Rancher server (which for us is also on the same subnet):

* ssh, http and https
* 2376 Docker TLS
* 2379 etcd
* 2380 etcd peer communication
* 6443 Kubernetes API
* 8472 Flannel VXLAN
* 10250 Kubelet
* 30000–32767 Nodeport port range

You will also need to expose the Kubernetes API (6443) publicly on your master node or wherever your load balancer is (the group is called `k8s-master` for LINCS). Please have a look [here](https://docs.ranchermanager.rancher.io/getting-started/installation-and-upgrade/installation-requirements/port-requirements) for a detailed description of port requirements.

## Setting up Rancher

Log into your Rancher portal and go to the Tools menu and select the Node Drivers tab. Activate the OpenStack node driver. The next step is to create one or more node templates to specify the node parameters when you create nodes for your cluster. These templates are saved against your user account and are available for all your clusters. Click on your user icon in the top right and select the Node Templates option. Click Add Template and select the OpenStack box. You’ll be presented with a form with many parameters but you need only enter the following fields (substitute the values as necessary).

* "authUrl": "https://arbutus.cloud.computecanada.ca:5000/v3 | https://cedar.cloud.computecanada.ca:5000/v3",
* "domainName": "CCDB",
* "flavorName": "c2-7.5gb-36 | c4-15gb-83 | c8-30gb-186 | c8-60gb-186 | c16-60gb-576 | c16-90gb-576 | c16-120gb-576 | c16-180gb-576",
* "imageName": "Ubuntu-22.04-Jammy-x64-2022-08",
* "ipVersion": "4",
* "netId": "af61b790-80a3-432e-84c6-df56f8c99e6f | dbf30084-c239-45ca-9fe4-a0a4614c8fb6",
* "secGroups": "default,Web server,k8s-node",
* "sshPort": "22",
* "sshUser": "ubuntu",
* "tenantId": "91cef58e96444b1abc6abdf09f9a9bf7 | 536c0c8ef53e4d0da3cdde8cac4bde78",
* "username": "zacanbot"

Give your template a name and hit the Create button. Create a template for each of the flavors you are going to use. We’re now ready to create our cluster.

## Create an RKE cluster with Rancher

I would recommend at this stage to open a terminal to the machine where Rancher is installed and use the log tail command `sudo kubectl -n cattle-system logs -l app=rancher -f | grep -v "Updating\|eflector\|Trace\["` to monitor what Rancher does in the background. Any problems during the cluster install will be highlighted in these logs.

Click on Global in the menu bar to go back to the home screen and then click the Add Cluster button on the top right. Select the OpenStack box under New nodes in an infrastructure provider. Give your cluster a name, your node pool a name prefix (Rancher will append a counter after this name), and select the node template in the dropdown box that you saved in your profile earlier. Also tick all three roles: etcd, Control Plane, and Worker. Select Kubernetes v1.20 and specify `/mnt/docker` as the Docker Root directory under *Advanced Options* and then click the Edit as YAML to the right of Cluster Options on the top right of the form.

This is where we tell Rancher how to integrate with OpenStack. Replace the rancher_kubernetes_engine_config element:

```yaml
rancher_kubernetes_engine_config:
  ignore_docker_version: true
  addon_job_timeout: 30
  authentication:
    strategy: x509
    sans:
      - 206.12.92.45 # Your load balancer/ingress IP
      - lincsproject.ca # Your public DNS name
  cloud_provider:
    name: "openstack"
    openstackCloudProvider:
      global:
        auth-url: 'https://arbutus.cloud.computecanada.ca:5000/v3' # Auth URL
        domain-name: CCDB # Identity v3 Domain Name
        tenant-id: 91cef58e96444b1abc6abdf09f9a9bf7 # Project ID
        username: 'zacanbot'
        password: 'myOSpassword'
      block_storage:
        bs-version: v2
        ignore-volume-az: true
        trust-device-path: false
  ingress:
    node_selector:
      kubernetes.io/hostname: prod-1 # node with external IP address
  services:
    kubelet:
      extra_args:
        max-pods: '250'
```

When using Rancher v2.6+ UI with rke2 cluster creation, use the *Additional Kubelet Args* under *Advanced* in the *Cluster Configuration* form, and add: `max-pods=250`.

We did not specify network parameters as it was done [here](https://medium.com/@zacanbot/kubernetes-with-rancher-on-openstack-f4bfb7985869) because CC does not have Octavia enabled nor Load Balancers for their users in OpenStack. The sans option under Authentication is critical for the K8s certificate authority to create certificates that potential clients will trust. Add all (or at least one) endpoint that you will use to connect to your cluster there or some clients might not trust the CA (gitlab.com is one such an example).

Now hit the Create button on the bottom and watch the Rancher log tail for ~10 minutes to see what Rancher does to create the new cluster on the OpenStack infrastructure. Since we don't have load balancing enabled, we need to set the public IP (the one specified above under authentication/sans) on the node in the OpenStack Horizon portal as soon as Rancher spins it up.

## Connecting to the cluster with Kubectl

You will need to configure `kubectl` to work with the new cluster from a shell. Follow the guide here: [setup_kubectl.md](docs/setup_kubectl.md).

## Storage

We need to add the Longhorn storage class after the cluster was created. Refer to the [storage_guide.md](docs/storage_guide.md) document.

## Ingress configuration

Since we don't have a load balancer provider that will dish out IP addresses for each load balancer request, we need to configure Ingress on one IP address. The [ingress_config.md](docs/ingress_config.md) document describes the process to get ingress and TLS termination working.

## Node Maintenance

There are some reasons to log into the virtual machines via SSH for maintenance and configuration that can't be done via the Rancher front-end. Refer to the guide here: [node_maintenance.md](docs/node_maintenance.md).

## Debug Networking

### Using an Ubuntu image

You can install a bare-bones Ubuntu container in the cluster to debug Networking from inside the cluster.

```bash
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: deployment-ubuntu
  name: ubuntu
  namespace: default
spec:
  selector:
    matchLabels:
      app: deployment-ubuntu
  template:
    metadata:
      labels:
        app: deployment-ubuntu
    spec:
      containers:
      - image: ubuntu:22.04
        name: ubuntu
        stdin: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        tty: true
EOF
```

Get the exact pod name with `kubectl get all -n default`. You can then open a terminal to the pod with:

```bash
kubectl exec --stdin --tty <POD_NAME> -- /bin/sh
```

Run these install commands inside the container to get some useful network tools available:

```bash
apt-get update
apt-get install iputils-ping dnsutils curl
```

If you've changed something inside the cluster and want to restart the pod, scale it to 0 and then again to 1.

```bash
kubectl scale deployment ubuntu --replicas=0 -n default
```

### Directly from the command line with kubectl

You can execute commands against the cluster by spinning up and then removing a bare-bones container. Here is an example to test external DNS:

```bash
kubectl run -it --rm --restart=Never swiss-army-knife --image=rancherlabs/swiss-army-knife -- nslookup www.google.com
```

### Using Rancher scripts

To test overlay networking across nodes, use this [guide](https://rancher.com/docs/rancher/v2.6/en/troubleshooting/networking/).

```yaml
cat <<EOF | kubectl apply -f -
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: overlaytest
spec:
  selector:
      matchLabels:
        name: overlaytest
  template:
    metadata:
      labels:
        name: overlaytest
    spec:
      tolerations:
      - operator: Exists
      containers:
      - image: rancherlabs/swiss-army-knife
        imagePullPolicy: Always
        name: overlaytest
        command: ["sh", "-c", "tail -f /dev/null"]
        terminationMessagePath: /dev/termination-log
EOF
```

```bash
kubectl rollout status ds/overlaytest -w
```

```bash
echo "=> Start network overlay test"
  kubectl get pods -l name=overlaytest -o jsonpath='{range .items[*]}{@.metadata.name}{" "}{@.spec.nodeName}{"\n"}{end}' |
  while read spod shost 
    do kubectl get pods -l name=overlaytest -o jsonpath='{range .items[*]}{@.status.podIP}{" "}{@.spec.nodeName}{"\n"}{end}' |
    while read tip thost
      do kubectl --request-timeout='10s' exec $spod -c overlaytest -- /bin/sh -c "ping -c2 $tip > /dev/null 2>&1"
        RC=$?
        if [ $RC -ne 0 ]
          then echo FAIL: $spod on $shost cannot reach pod IP $tip on $thost
          else echo $shost can reach $thost
        fi
    done
  done
echo "=> End network overlay test"
```

```bash
kubectl delete ds/overlaytest
```
