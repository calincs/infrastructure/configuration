# Compute Canada resources

LINCS utilise IT infrastructure provided by Compute Canada to host its services. This document maps these resources to the specific LINCS functions they have been allocated to.

Some of the CWRC infrastructure is also used by LINCS and are included [here](docs/cwrc_servers.md).

The LINCS project on CC is `rpp-sbrown-lincs`. Currently, Susan Brown and Pieter Botha has access to this project.

Hosts run Ubuntu 18.04 except for the two worker nodes of the production cluster that is based on Debian Buster. SSH access is available by logging in with ubuntu@host using a stored SSH key, or debian@host for the debian hosts.

## Storage Configuration

Several volumes are defined for the LINCS K8s clusters and backup server. The [storage_guide.md](docs/storage_guide.md) document explains how the storage is configured on the actual hosts and exposed to the relevant services.

## VM hosts

### rancher.lincsproject.ca 206.12.90.224

Hostname: **rancher**

The Rancher server that provides Kubernetes as a service for LINCS. Rancher is installed in a K3s Kubernetes cluster on this host.

### aux.lincsproject.ca 206.12.95.211

Hostname: **backup**

Minio Server that hosts the S3 backup system for LINCS

### lincsproject.ca 206.12.92.45

Hostname: **prod-ctrl1**

This is the control pane and etcd backend of the production cluster that also handles all ingress.

### stage.lincsproject.ca 206.12.93.114

Hostname: **stage-ctrl1**

Etcd, control pane, and ingress controller for the LINCS staging cluster.

### sandbox.lincsproject.ca 206.12.93.100

Hostname: **sandbox1**

The entire sandbox single-node cluster is running on this host.

### vm1.lincsproject.ca 206.12.92.17

Hostname: **vm1**

A single-node development cluster for experimentation for LINCS developers.

### alpha.lincsproject.ca 206.12.92.201

Hostname: **alpha**

The GitLab-runner server that executes all LINCS CI jobs. It has 16 cores and 180GB RAM. It is also used for testing ingestion and other resource-intensive scripts that can benefit to be on the same network as the LINCS Kubernetes clusters.

## Other hosts

There are 2 worker nodes instantiated for the production cluster and 1 for the stage cluster. These nodes are managed via Rancher. For OS updates and other maintenance on Rancher k8s nodes, see this guide: [node_maintenance.md](docs/node_maintenance.md)
