# Minio S3 storage for LINCS

[https://aux.lincsproject.ca](https://aux.lincsproject.ca) hosts a Minio service for LINCS. This service is S3-compatible and can be accessed with almost any S3 tool or library. It is primarily used for [backups](docs/backups.md) and large file sharing.

There is a public user set up that has access to only the public bucket:

* key: **public**
* secret: **knockknock**


## Certificate renewal (NO wildcard cert)

The following script was created to ease the renewal process. It runs `certbot renew` and copies the key files with the correct permissions.

```bash
cat > renew-cert.sh <<EOF
#!/bin/sh
#certbot checks for new certs every day by default, so just copy the certs
certbot renew
sudo cp /etc/letsencrypt/live/aux.lincsproject.ca/fullchain.pem /etc/minio/certs/public.crt
sudo cp /etc/letsencrypt/live/aux.lincsproject.ca/privkey.pem /etc/minio/certs/private.key
sudo chown minio-user:minio-user /etc/minio/certs/public.crt
sudo chown minio-user:minio-user /etc/minio/certs/private.key
EOF
chmod +x renew-cert.sh
```

## Certificate renewal (WITH a wildcard cert)

Because the wildcard certificate requires a DNS challenge, and we need to create DNS TXT records in the process, it can't be automated with certbot. You have to manually create the certificate the same way as you did the first time:

```bash
sudo certbot certonly --manual \
  --manual-public-ip-logging-ok \
  --preferred-challenges dns-01 \
  --server https://acme-v02.api.letsencrypt.org/directory \
  -d "*.aux.lincsproject.ca" -d aux.lincsproject.ca
```

You will then get a prompt to create a specific DNS TXT record. Go to `namespro.ca` and create the record with their zone editing tool. You can test if the DNS record has propagated by going to [this](https://mxtoolbox.com/SuperTool.aspx?action=txt%3a_acme-challenge.aux.lincsproject.ca&run=toolpage) site, or by running the following command in another terminal:

```bash
dig -t TXT +short _acme-challenge.aux.lincsproject.ca
```

Once the TXT record is available (it takes anything from 30 minutes to 3 hours), continue the renewal process in the certbot terminal. You might need to go through the process twice.

Lastly, make sure that the `cerbot renew` line is commented out in the `renew-cert.sh` script and then run it, which will copy the certificate to the correct folder.

```bash
./renew-cert.sh
```

## The mc client

The `mc` client from Minio is a useful commandline tool to interact with the Minio service. To install:

```bash
wget https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
sudo mv ./mc /usr/bin
mc config host add lincs https://aux.lincsproject.ca <key> <secret>
```

## Administrating Minio

### Updates

To update the server, simply run:

```bash
mc admin update aux
```

### User-based access to the users bucket

```json
{
    "Version": "2012-10-17",
    "Statement": [
    {
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::users"],
       "Condition": {"StringEquals":{"s3:prefix":[""],"s3:delimiter":["/"]}}
    },
    {
       "Effect": "Allow",
       "Action": ["s3:ListBucket"],
       "Resource": ["arn:aws:s3:::users"],
       "Condition": {"StringLike":{"s3:prefix":["${aws:username}/*"]}}
    },
    {
       "Effect": "Allow",
       "Action": ["s3:*"],
       "Resource": ["arn:aws:s3:::users/${aws:username}/*"]
    }
    ]
}
```

### Public access for the public and dumps buckets

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:ListAllMyBuckets","s3:ListBucket"],
            "Resource": ["arn:aws:s3:::public"]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:ListMultipartUploadParts",
                "s3:PutObject"
            ],
            "Resource": ["arn:aws:s3:::public/*"]
        },
        {
            "Effect": "Allow",
            "Action": ["s3:ListAllMyBuckets","s3:ListBucket"],
            "Resource": ["arn:aws:s3:::dumps"]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:ListMultipartUploadParts",
                "s3:PutObject"
            ],
            "Resource": ["arn:aws:s3:::dumps/*"]
        }
    ]
}
```

## Creating new Minio users

The following script is useful to create new users and add them to the appropriate groups for user-level access.

```bash
cat > create-s3-user.sh <<EOF
#!/bin/sh
if [ $# -ne 2 ] ; then
    echo 'Specify username and password as parameters'
    exit 1
fi
mc admin user add lincs $1 $2
mc admin group add lincs users $1
mc admin group add lincs shared $1
echo "Download the MinIO mc client (or any other S3 client) to work with your files: https://docs.min.io/docs/minio-client-quickstart-guide.html" \
| mc pipe lincs/users/$1/welcome.txt
echo done
EOF
chmod +x create-s3-user.sh
```

Here is a nice welcome message that can be forwarded for new users:

```text
Our S3 storage is up. https://aux.lincsproject.ca
Access Key <user> with Secret Key <secret>
You have access to private storage under users/<user> and public buckets public and dumps
You can upload/download files directly in the browser and change your password
You can also use any S3-compatible client to access the buckets. Minio's mc works best imho: https://docs.min.io/docs/minio-client-complete-guide.html
```
