# The LINCS Wordpress site

The [LINCS website](https://lincsproject.ca) is hosted in the LINCS *production* Kubernetes cluster by Wordpress.

## Backup and Restore

We use the *All-in-One WP Migration* plugin to export/import the entire site as a file. The plugin can be installed from the Wordpress admin pages and needs another plugin: *All-in-One WP Migration File Extension* to allow file sizes up to 500MB. You need to install the basic version of this plugin, which is not available from the author's site anymore but can be found in the [resources](../resources/all-in-one-wp-migration-file-extension.zip) folder of this repo. To create a backup, simply select *Export* from the *All-in-One WP Migration* menu and then choose *File* as the export type. Download the file and upload it to the [LINCS S3 bucket](https://aux.lincsproject.ca/minio/lincs/).

Restoring a backup just involves choosing the *Import* option from the *All-in-One WP Migration* menu and then selecting the backup file.

## Wordpress setup

Installing the website involves installing Wordpress from an official Helm chart. Use the Bitnami Wordpress chart. The current configuration was tested with v13.1.0 of the chart:

```yaml
ingress:
  annotations:
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "300"    
  enabled: true
  extraTls:
  - secretName: www-tls-secret
    hosts:
    - www.lincsproject.ca
  hostname: www.lincsproject.ca
  path: /
  pathType: ImplementationSpecific
  tls: true
mariadb:
  auth:
    database: bitnami_wordpress
    password: <thePassword>
    rootPassword: <thePassword>
    username: bn_wordpress
service:
  type: ClusterIP
wordpressBlogName: LINCS Blog
wordpressEmail: lincs@uoguelph.ca
wordpressFirstName: Lincs
wordpressLastName: Project
wordpressPassword: <thePassword>
wordpressUsername: user
```

Previous Wordpress charts in the Rancher library didn't install ingress correctly in the LINCS cluster. Just replace the *service* and *ingress* with the manifests below after the Wordpress app was installed with Rancher:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: wordpress-lincs
  namespace: wordpress-lincs
spec:
  ports:
  - port: 8080
    name: http
    targetPort: 8080
  - port: 8443
    name: https
    targetPort: 8443
  type: ClusterIP
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  labels:
    app.kubernetes.io/instance: wordpress-lincs
    io.cattle.field/appId: wordpress-lincs
  name: wordpress-lincs
  namespace: wordpress-lincs
spec:
  rules:
  - host: www.lincsproject.ca
    http:
      paths:
      - backend:
          service:
            name: wordpress-lincs
            port:
              name: http
        path: /
        pathType: ImplementationSpecific
  - host: lincsproject.ca
    http:
      paths:
      - backend:
          service:
            name: wordpress-lincs
            port:
                name: http
        path: /
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - www.lincsproject.ca
    - lincsproject.ca
    secretName: tls-secret
```

## Fix access

We need to enable access to `wp-config.php` for some plugins to work. Open a shell to the Wordpress pod and run this:

```bash
chmod 660 /bitnami/wordpress/wp-config.php
```
