# CephFS storage setup

We tried many ways to connect to the CephFS shares on Arbutus:

* the official [ceph-csi](https://github.com/ceph/ceph-csi) driver
* the old [cephfs-provisioner](https://github.com/elemental-lf/external-storage)
* the Kubernetes in-tree volume provisioner for cephfs
* cephfs manual mounts with [Longhorn](https://longhorn.io/)
* cephfs manual mounts with Rancher's local path [provisioner](https://github.com/rancher/local-path-provisioner/)

Only the last option seems like a viable solution.

Pros over Longhorn-on-Cinder:

* 10x faster
* it actually works and doesn't generate IO errors

Cons:

* single volume replicas only
* not a distributed filesystem (pod must be on the node with the volume)
* no Longhorn admin features (backups, replica management, node distribution, etc.)

Still to consider: I am not sure if it will be possible to get the [Rook](https://rook.io/docs/rook/v1.8/ceph-filesystem.html) csi driver working on the Arbutus setup.

## Creating CephFS shares

Before installing any k8s drivers, we need to create the CephFS shares in the Arbutus Horizon front-end. Follow the guide [here](https://docs.computecanada.ca/wiki/Arbutus_CephFS_user_guide) to create the `share` and the `share rule`. By going to the detail page of the share, you'd be able to see the information needed to connect k8s to the CephFS share.

Options for creating the share:

* Name = a name that identifies your project: lincs-dev
* Share Protocol = cephfs
* Size = size you need for this share
* Share Type = cephfs
* Availability Zone = nova
* Do not check "Make visible for all", otherwise the share would be accessible by anyone in every project.

On the "Shares" pane, click on the drop down menu under "Actions" and select "Manage Rules". Create a new rule using the "+Add Rule" button.

* Access Type = cephx
* Select "read-write" or "read-only" under "Access Level". You can create multiple rules for either access level if required.
* Choose a key name in the "Access To" field that describes the key (e.g. lincs-dev-rw).

The Ceph monitors can be read from the `path` property on the share detail page, while the adminID and adminKey for the ceph secret can be taken from `Access to` and `Access Key` fields on the share rule. I don't think we have a reference for clusterID in the Arbutus setup.

## ceph-csi

Add the ceph-csi helm repo: `https://ceph.github.io/csi-charts` and then install the `ceph-csi-cephfs` helm chart.

Customise values.yaml with the following changes:

```yaml
csiConfig: [
    {
      "clusterID": "79c48de9-4d61-4048-9e33-ec9c823eb2b3",
      "monitors": [
        "10.30.201.3:6789",
        "10.30.202.3:6789",
        "10.30.203.3:6789"
      ]
    }
  ]
secret:
  adminID: lincs-dev-rw
  adminKey: AQD+reBhb2SiCRAA2e7l9az+8D8FVwrggVxm9A==
  create: true
  name: csi-cephfs-secret
storageClass:
  allowVolumeExpansion: true
  clusterID: 79c48de9-4d61-4048-9e33-ec9c823eb2b3
  create: true
  fsName: lincs-dev-a
  provisionerSecret: csi-cephfs-secret
```

I could not get this approach to work because I think we need admin access to the Ceph cluster.

## cephfs-provisioner

Here is a good [guide](https://computingforgeeks.com/ceph-persistent-storage-for-kubernetes-with-cephfs/) to setup the provisioner. Just be sure to change the provisioner image from `quay.io/external_storage/cephfs-provisioner:latest` to `elementalnet/cephfs-provisioner:0.8.0`, which supports k8s beyond v1.19.

```yaml
cat <<EOF | kubectl apply -f -
---
kind: Namespace
apiVersion: v1
metadata:
  name: cephfs
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cephfs-provisioner
  namespace: cephfs
rules:
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "update", "patch"]
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["kube-dns","coredns"]
    verbs: ["list", "get"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cephfs-provisioner
  namespace: cephfs
subjects:
  - kind: ServiceAccount
    name: cephfs-provisioner
    namespace: cephfs
roleRef:
  kind: ClusterRole
  name: cephfs-provisioner
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: cephfs-provisioner
  namespace: cephfs
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["create", "get", "delete"]
  - apiGroups: [""]
    resources: ["endpoints"]
    verbs: ["get", "list", "watch", "create", "update", "patch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cephfs-provisioner
  namespace: cephfs
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: cephfs-provisioner
subjects:
- kind: ServiceAccount
  name: cephfs-provisioner
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: cephfs-provisioner
  namespace: cephfs
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cephfs-provisioner
  namespace: cephfs
spec:
  replicas: 1
  selector:
    matchLabels:
      app: cephfs-provisioner
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: cephfs-provisioner
    spec:
      containers:
      - name: cephfs-provisioner
        image: "elementalnet/cephfs-provisioner:0.8.0"
        env:
        - name: PROVISIONER_NAME
          value: ceph.com/cephfs
        - name: PROVISIONER_SECRET_NAMESPACE
          value: cephfs
        command:
        - "/usr/local/bin/cephfs-provisioner"
        args:
        - "-id=cephfs-provisioner-1"
      serviceAccount: cephfs-provisioner
EOF
```

```bash
kubectl create secret generic ceph-admin-secret \
    --from-literal=key='AQD+reBhb2SiCRAA2e7l9az+8D8FVwrggVxm9A==' \
    --namespace=cephfs
```

```yaml
cat <<EOF | kubectl apply -f -
---
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: cephfs
  namespace: cephfs
provisioner: ceph.com/cephfs
parameters:
    monitors: 10.30.201.3:6789,10.30.202.3:6789,10.30.203.3:6789
    adminId: lincs-dev-rw
    adminSecretName: ceph-admin-secret
    adminSecretNamespace: cephfs
    claimRoot: /volumes/_nogroup/e647c4b6-46b6-4b73-b4ae-d1cfe76c94ac
EOF
```

This also fails with insufficient privileges. Probably requires admin access too.

```text
cephfs.PermissionError: error calling ceph_mount: Operation not permitted
```

## in-tree volume provisioner

You can directly mount a CephFS share inside a pod with the cephFS volume provisioner built into k8s. E.g.:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test
  namespace: default
spec:
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: apps.deployment-test
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: apps.deployment-test
    spec:
      containers:
      - image: ubuntu:focal
        imagePullPolicy: Always
        command:
        - /bin/sh
        - -c
        - sleep 50000
        name: cephfs-rw
        volumeMounts:
        - mountPath: /mnt/cephfs
          name: cephfs
      volumes:
      - cephfs:
          monitors:
          - 10.30.201.3:6789
          - 10.30.202.3:6789
          - 10.30.203.3:6789
          path: /volumes/_nogroup/e647c4b6-46b6-4b73-b4ae-d1cfe76c94ac
          secretRef:
            name: csi-cephfs-secret
          user: lincs-dev-rw
        name: cephfs
```

This approach has the limitation that you have to manually create a share for each volume you want to map in a pod. This won't work for our CI pipeline.

## Manually mounted CephFS share

Follow these instructions to mount a cephfs share inside a node: (replace key and share values in the example)

```bash
wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
sudo apt-add-repository 'deb https://download.ceph.com/debian-nautilus/ focal main'
sudo apt-get install libcephfs2 ceph-common
sudo nano /etc/ceph/client.fullkey.lincs-dev-a
```

```text
[client.shareName]
  key = AQBp9eFhbE7FDRAAbGn+xx4Vaq5XsK3TIy9xOA==
```

```bash
sudo nano /etc/ceph/client.keyonly.lincs-dev-a
```

```text
AQBp9eFhbE7FDRAAbGn+xx4Vaq5XsK3TIy9xOA==
```

```bash
sudo chmod 600 /etc/ceph/client.fullkey.lincs-dev-a
sudo chmod 600 /etc/ceph/client.keyonly.lincs-dev-a
sudo nano /etc/ceph/ceph.conf
```

```text
[client]
  client quota = true
  mon host = 10.30.201.3:6789,10.30.202.3:6789,10.30.203.3:6789
```

```bash
sudo mkdir /cephfs
sudo mount -t ceph 10.30.201.3:6789,10.30.202.3:6789,10.30.203.3:6789:/volumes/_nogroup/0ce1df88-5d0e-49db-a763-afe0de25c003 /cephfs -o name=lincs-dev-a-rw,secretfile=/etc/ceph/client.keyonly.lincs-dev-a
```

### Longhorn over the mounted cephfs volumes

Unfortunately, cephfs doesn't support all the features Longhorn requires to provision storage. Officially, Longhorn only supports the ext4 and xfs file systems. NFS also does not work with Longhorn.

### Local path provisioner on cephfs mounts

Rancher's local path [provisioner](https://github.com/rancher/local-path-provisioner/) is another alternative we tried to utilise the cephfs shares. This seems to be the only solution that will work for us.

Just install from the helm chart in the repo in the `local-path-storage` namespace with default values and only change the `nodePathMap` to point to the `/cephfs` mount inside the nodes.

### Test pvc and pod

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc
  namespace: default
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: local-path
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      workload.user.cattle.io/workloadselector: apps.deployment-test
  template:
    metadata:
      labels:
        workload.user.cattle.io/workloadselector: apps.deployment-test
    spec:
      containers:
      - image: ubuntu:focal
        imagePullPolicy: Always
        command:
        - /bin/sh
        - -c
        - sleep 50000
        name: test-disk
        volumeMounts:
        - mountPath: /mnt
          name: cephfs
      volumes:
      - name: cephfs
        persistentVolumeClaim:
          claimName: test-pvc
```
