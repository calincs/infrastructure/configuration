# HashiCorp Vault configuration

Vault has been installed with the LINCS cluster management [project](https://gitlab.com/calincs/infrastructure/cluster-management) based on GitLab CI/CD. It is available at [https://vault.lincsproject.ca/](https://vault.lincsproject.ca/).

## Installation

Just adding vault in the CI/CD project configuration will only enable access to Vault from within the cluster at `vault.gitlab-managed-apps:8200`. In order to get access from outside the cluster, we need to create an Ingress for the service manually:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: vault-ingress
  namespace: gitlab-managed-apps
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  labels:
    app.kubernetes.io/instance: vault
spec:
  rules:
  - host: vault.lincsproject.ca
    http:
      paths:
      - path: /
        pathType: ImplementationSpecific
        backend:
          service:
            name: vault-ui
            port:
              number: 8200
  tls:
  - hosts:
    - vault.lincsproject.ca
    secretName: vault-ui-cert
```

## Setup

After Vault has been added to the cluster, it won't be usable until it gets initialized and unsealed.

### Open a shell with the Vault CLI tool

Firstly, we need to get access to a shell with the Vault CLI tool connected to our cluster.

#### Connect using the Vault container in the LINCS cluster

If you have the correct *kube config* already set up for `kubectl`, you can simply run this to get a shell prompt to the vault container in the cluster

```bash
kubectl -n gitlab-managed-apps exec -it vault-0 -- sh
```

#### Connect with your own Docker container

Alternatively, you can start a command prompt from a Vault docker image:

```bash
docker run -it vault:latest /bin/sh
```

You then need to tell the CLI where the Vault server is with an environment variable:

```bash
export VAULT_ADDR=https://vault.lincsproject.ca
```

### Initialize and unseal the vault

The following command will initialize Vault and give the initial root token along with 5 key shares to be used to unseal the vault.

```bash
vault operator init
```

Vault is initialized with 5 key shares and a key threshold of 3 by default. **When the Vault is re-sealed, restarted, or stopped, you must supply at least 3 of these keys to unseal it before it can start servicing requests again**. Vault does not store the generated master key. Without at least 3 keys to reconstruct the master key, Vault will remain permanently sealed!

Unseal the vault now by running this 3 times supplying a different key share every time:

```bash
vault operator unseal
```

You can login with the initial root token to administrate Vault:

```bash
vault login <root_token>
```

### Enable JWT authentication and GitLab config

To make GitLab ci pipelines work with Vault, we need to enable JWT authentication and register the GitLab issuer:

```bash
vault auth enable jwt
vault write auth/jwt/config jwks_url="https://gitlab.com/-/jwks" bound_issuer="gitlab.com"
```

Also create the `VAULT_SERVER_URL` CI variable on the top group-level so that all projects will have it automatically. Set it to `https://vault.lincsproject.ca`.

You can now set up Vault policies for individual LINCS projects as explained in the guide here: [vault_usage.md](docs/vault_usage.md)

## Other notes

Official documentation can be followed out from here: [Installing Vault with GitLab CI/CD](https://docs.gitlab.com/ee/user/clusters/applications.html#install-vault-using-gitlab-cicd) and [Using external secrets in CI](https://docs.gitlab.com/ee/ci/secrets/index.html)

A super-user policy can be created like so:

```bash
vault policy write super - <<EOF
path "*" {
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
EOF
```
