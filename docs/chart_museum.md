# Chart Museum for LINCS

A Helm chart is an easy way to package an application for deployment in Kubernetes. It is the preferred way to package LINCS projects and allows easy installation of the packaged right from the Rancher UI e.g. [LINCS Wikibase](https://gitlab.com/calincs/infrastructure/wikibase-chart). Chart Museum is a repository for Helm charts and LINCS has one deployed in the production k8s cluster.

## Chart Museum setup

Chart Museum comes with one of the default Rancher catalogs and can be installed via the *Apps* tab in any project on your cluster. We need to store the username and password for uploading charts in a k8s secret. This can be done via the Rancher UI under the *Resources* menu, or directly with `kubectl`:

```bash
kubectl create namespace chartmuseum
kubectl -n chartmuseum create secret generic chartmuseum-auth --from-literal=user=curator --from-literal=pass=lincsmayenter
```

When presented with the form after launching the ChartMuseum app from the Rancher library, be sure to set the namespace and specify the secret. Also set the following parameters in the *Values* section of the form:

```text
env.open.DISABLE_API=false
env.open.AUTH_ANONYMOUS_GET=true
env.open.AUTH_REALM=chartmuseum
existingSecret=chartmuseum-auth
existingSecretMappings.BASIC_AUTH_USER=user
existingSecretMappings.BASIC_AUTH_PASS=pass
```

Choose to not let the chart create the ingress, and create it manually:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  labels:
    app: chartmuseum
    chart: chartmuseum-2.3.1
    heritage: Tiller
    io.cattle.field/appId: chartmuseum
    release: chartmuseum
  name: chartmuseum-chartmuseum
  namespace: chartmuseum
spec:
  rules:
  - host: charts.lincsproject.ca
    http:
      paths:
      - backend:
          service:
            name: chartmuseum-chartmuseum
            port:
              number: 8080
        path: /
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - charts.lincsproject.ca
    secretName: tls-secret
```

Finally, add the repo to the Rancher catalogs so that all deployed charts will be available as Apps: https://charts.lincsproject.ca

## Posting Helm charts to ChartMuseum

There is a plugin for chartmuseum that allows Helm to push a chart to a repository. Assuming that Helm 3 is already installed, you can execute the following commands to upload a chart to a Chart Museum instance called *chartmuseum* at `https://charts.lincsproject.ca/`:

```bash
helm plugin install https://github.com/chartmuseum/helm-push.git
helm repo add --username curator --password lincsmayenter chart-museum https://charts.lincsproject.ca/
helm push charts/wikibase chart-museum
```

Please look at the [Wikibase Chart](https://gitlab.com/calincs/infrastructure/wikibase-chart) project for the file structure that is expected in the charts/wikibase folder for the example above.
