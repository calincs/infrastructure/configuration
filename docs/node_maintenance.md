# Kubernetes node install and maintenance

The LINCS k8s clusters are managed from Rancher for the most part. But there might be rare cases where one would need to maintain specific node. This might include OS updates, storage configuration, or DNS maintenance. Adding a new node will also require one to connect, update the host, and adjust DNS settings.

The current new node preparation detailed below can be done by running a single script. Have a look at the `prepare_node.sh` file in the resource folder of this repo.

## Connecting to a Node

1. Assign a public IP address to the node. Choose to `associate a floating IP` to the node from the drop-down menu on the host in the OpenStack Horizon interface after logging in to [https://arbutus.cloud.computecanada.ca](https://arbutus.cloud.computecanada.ca) and going to the `Instances` page.

2. Download the node certificates. Log into [https://rancher.licnsproject.ca](https://rancher.licnsproject.ca) and select the cluster and then the Nodes tab. To the right of the node click on the menu icon and select to download certificates. Extract the certificates somewhere on your local machine.

3. Log in with SSH. Log into the node by specifying to SSH where the certificate is that you downloaded, e.g. `ssh ubuntu@the.ip.address -i C:\Users\zacan\.ssh\prod-2\id_rsa`

4. Do your maintenance, e.g.

```bash
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt autoremove
```

## Debian additional requirements

If you use a Debian image instead of an Ubuntu image for a node, some additional configuration steps are required:

```bash
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
sudo apt-get install -y open-iscsi lvm2 htop
```

## RKE2 configuration

When creating an RKE2 cluster with a Rancher Agent link from the Rancher UI, it is advisable to create a symbolic link to change the default RKE2 data directory (until the option is added to the GUI)

```bash
cd /var/lib/rancher
sudo rke2-killall.sh
sudo cp -a rke2 /mnt/rke2
sudo rm rke2/ -rf
sudo ln -s /mnt/rke2 rke2
sudo reboot
```

## Install Docker

### Install on Ubuntu 24.04

```bash
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
sudo apt update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
# if GPG error: sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo groupadd docker
sudo usermod -aG docker $USER
# log out and log back in. Then:
newgrp docker
rm -r ~/.docker
```

### Move Docker storage location

```bash
sudo systemctl stop docker
```

Create/edit the configuration at `/etc/docker/daemon.json`:

```json
{
"data-root": "/mnt/docker"
}
```

Copy over the current Docker data and restart:

```bash
sudo mkdir -p /mnt/docker
sudo cp -axT /var/lib/docker /mnt/docker
sudo systemctl start docker
```

Check if everything works and remove the original directory:

```bash
sudo docker images
sudo rm -R /var/lib/docker
```

### Ensure mount points are up

To make sure that Docker starts after volumes are mounted, you need to make the service depend on the mounts. Edit the service by running:

```bash
sudo systemctl edit --full docker
```

In the `Unit` section, add this:

```bash
RequiresMountsFor=/mnt /longhorn
```

### Purge Docker storage

```bash
sudo docker stop `sudo docker ps -qa` > /dev/null 2>&1 ## Stop all running containers
sudo docker system prune --volumes --all ## Remove all unused docker components
```

## Compute Canada DNS "problem"

Linux only supports up to 3 DNS addresses to resolve domain names. Compute Canada, however, currently supplies 4 addresses to all hosts via DHCP. In addition to the Cluster DNS server, all k8s Pods now have 5 DNS addresses to choose from. Kubernetes doesn't like this one bit. It doesn't break name resolution as the additional hosts are just ignored, but the k8s logs get flooded with warning messages. To fix the problem, we can specify only two DNS hosts for the host manually.

### Manual DNS for Debian images

```bash
sudo nano /etc/dhcp/dhclient.conf
```

Then add this line in there:

```bash
supersede domain-name-servers 192.168.211.4, 192.168.211.3;
```

Save and restart.

### Manual DNS for Ubuntu images

```bash
sudo nano /etc/netplan/50-cloud-init.yaml
```

Then add this just below the `dhcp4: true` value on the same indentation:

```yaml
            dhcp4-overrides:
                use-dns: false
            nameservers:
                search: [arbutus]
                addresses: [192.168.211.4, 192.168.211.3]
```

Run the following command to apply the new setting:

```bash
sudo netplan --debug apply
```

Please note that the node **must** have all the latest updates installed for this to work as the dhcp override was only added recently to the *netplan* package.

## Set correct time zone

On Ubuntu 20.04, you can run the following command to set the timezone:

```bash
sudo timedatectl set-timezone America/Toronto
```

## Disable multipathd for Longhorn

```bash
sudo nano /etc/multipath.conf
```

```bash
blacklist {
    devnode "^sd[a-z0-9]+"
}
```

```bash
sudo systemctl restart multipathd.service
```

## Removing clusters that are "stuck"

Connected to the Rancher cluster:

```bash
kubectl get clusters.management.cattle.io  # find the cluster you want to delete 
export CLUSTERID="c-xxxxxxxxx"
kubectl patch clusters.management.cattle.io $CLUSTERID -p '{"metadata":{"finalizers":[]}}' --type=merge
kubectl delete clusters.management.cattle.io $CLUSTERID
```
