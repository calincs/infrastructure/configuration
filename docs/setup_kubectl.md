# Setting up Kubectl

This document will guide you through the process of getting `kubectl` to work on your Rancher k8s cluster. Kubectl is the official CLI tool for working with Kubernetes clusters. These instruction are for Ubuntu but you can translate them to Windows or other distros.

## Installing Kubectl

To get access to the LINCS k8s cluster you will need Rancher access. The roles and authorisations defined for your Rancher user will also be applied when using `kubectl` with the kubeconfig from Rancher.

Let's install kubectl first. Other environments look [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/) for instructions.

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
kubectl version --client
```

## Configuring Kubectl

Now we need to tell `kubectl` to connect to the cluster. Log into [Rancher](https://rancher.lincsproject.ca/) and click on your cluster in the *Global* menu. If you can't see it, it means that your user doesn't have the correct access. Make sure you're on the *Cluster* tab and then click the *Kubeconfig File* button. Hit the *Copy to Clipboard* link on the bottom of that window.

Create the kubeconfig by first running `mkdir ~/.kube` and then `nano ~/.kube/config`. Then hit *shift-insert* to paste the content and then *ctrl-x + y* to save. Check that you are connecting to the correct cluster:

```bash
kubectl config view
```
