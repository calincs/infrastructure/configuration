# Using HashiCorp Vault in LINCS GitLab projects

Gitlab has integrated much of Hashicorp's Vault right into GitLab CI and made it fairly simple to access secrets. GitLab authenticates with JWT tokens built for your CI pipeline. But in order to use Vault secrets with your project, you will still need to create a policy for your project and assign that policy to a role.

Vault is configured and ready to use in the LINCS environment. But there are a few things that need to be done for each project before it can access vault secrets:

1. Create the secrets
2. Create a policy
3. Assign the policy to a project-bound role
4. Implement the CI code in your `.gitlab.yml` file

## 1. Create the secrets

There is already a key-value (KV) engine configured in Vault called `lincs` that you can use for your project. For other types of values e.g. key files, please refer to the official Vault [documentation](https://learn.hashicorp.com/tutorials/vault/getting-started-first-secret) for instructions on how to create secret engines and secrets.

### Create secrets with the web UI

This is the simplest way to add secrets. Go to [https://vault.lincsproject.ca](https://vault.lincsproject.ca) and log in with your credentials. Go to the *Secrets* page and click on the `lincs` secret engine. All the projects you have access to will be listed here. Click on the *Create secret* link on the right to open the secret form.

You should enter the path to your secret at least two levels deep. The first level will reference your project and the second a folder to keep the secrets in e.g. *my-project/secrets*

Go ahead and add a key-value pair and click the *Add* button. Add more key-value pairs if you want to. Click *Save* on the bottom to save the secrets in Vault.

If you saved a *password* key, the value will be available at lincs/my-project/secrets, under the field *password*.

### Create secrets with a Vault prompt

You can also work with the Vault API through the CLI tools via a command prompt. This is the suggested way since most documentation also refers to the CLI. To get access to Vault CLI, you can install it for your platform, or just run a prompt in `docker`:

```bash
docker run -it vault:latest /bin/sh
```

You then need to authenticate to the Vault server to get a token to be able to issue any commands. You can login with a token directly or with your username/password:

```bash
export VAULT_ADDR=https://vault.lincsproject.ca
vault login -method=userpass username=theuser password=thepassword
```

You can now go ahead and create your key:

```bash
vault kv put lincs/my-project/secrets password=youwillneverguessthisone
```

Read back the key with `kv get`:

```bash
vault kv get -field=password lincs/my-project/secrets
```

## 2. Create a Policy

You need to allow your project to read the secrets:

```bash
vault policy write my-project-read - <<EOF
path "lincs/data/my-project/*" {
  capabilities = [ "read" ]
}
EOF
```

The `/data` in that path is not a typo.

## 3. Create a Role

You can use roles to control who gets access to your policy by using `bound_claims`. This simple role will allow access only to a JWT token with your project's ID:

```bash
vault write auth/jwt/role/my-project-read - <<EOF
{
  "role_type": "jwt",
  "policies": ["my-project-read"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_login",
  "bound_claims_type": "string",
  "bound_claims": {
    "project_id": "123456"
  }
}
EOF
```

Replace `123456` with your GitLab project's ID.

## 4. Implement the CI code to use the secrets

This part is nicely integrated into the GitLab CI. The following example `.gitlab.yml` shows how to use the *password* variable created above. Note that the secret is similar to a GilLab project variable of type *file*.

```yaml
job:
  stage: pre_deploy
  variables:
    VAULT_AUTH_ROLE: my-project-read
  secrets:
    VAULT_TEST:
      vault: my-project/secrets/password@lincs
# translates to secret `lincs/my-project/secrets`, field `password`
  script:
    - cp $VAULT_TEST testFile
    - cat testFile
```

## Other notes

You can also access the vault API from within the LINCS k8s cluster. A pod won't have to go out and back in over the ingress to access the Vault server at `https://lincsproject.ca` but can connect directly to `vault.gitlab-managed-apps:8200` from inside the cluster.

You may also want to have a look at the official GitLab documentation here: [Using secrets in a job](https://docs.gitlab.com/ee/ci/yaml/README.html#secretsvault)
