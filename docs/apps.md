# Other LINCS Applications

## Weblate

Install the `weblate` chart from `https://helm.weblate.org` into the `weblate` namespace. Find the secret values in the project CI variables of this repository.

Customize these values for the install:

```yaml
adminEmail: zacanbot@gmail.com
adminPassword: <WEBLATE_ADMIN_PASSWORD>
adminUser: zacanbot
defaultFromEmail: lincs.project@gmail.com
emailHost: smtp.gmail.com
emailPassword: <LINCS_SMTP_KEY>
emailPort: 587
emailSSL: false
emailTLS: true
emailUser: lincs.project@gmail.com
extraConfig:
  GITLAB_CREDENTIALS: |
    {
      "https://gitlab.com": {
        "username": "zacanbot",
        "token": "<ZACANBOT_GITLAB_TOKEN>",
      },
    }
  WEBLATE_REGISTRATION_OPEN: "0"
ingress:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  enabled: true
  hosts:
  - host: weblate.lincsproject.ca
    paths:
    - path: /
      pathType: Prefix
  ingressClassName: nginx
  tls:
  - hosts:
    - weblate.lincsproject.ca
    secretName: tls-secret
postgresql:
  image:
    tag: 16.4.0
  primary:
    extraEnvVars:
      - name: POSTGRESQL_REPLICATION_USE_PASSFILE
        value: "no"
service:
  type: ClusterIP
serviceAccount:
  create: true
siteDomain: weblate.lincsproject.ca
```
