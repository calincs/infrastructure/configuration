# LINCS backups

## GitLab

All repositories are backed up to the `gitlab` bucket on our Minio server. The following script downloads all projects and saves them to S3 storage and is currently scheduled with a cron job on the Minio VM:

```bash
#!/bin/sh

# backs up all project that has been updated in the last day

# dirname=gitlab-backup-$(date "+%Y-%m-%d")
dirname=ziptmp
mkdir -p "$dirname"
cd $dirname

privateToken=glpat-Rssfga5iLg7WMgZCcyfA
yesterday=$(date -d "$date -1 days" +"%Y-%m-%d")

curl --header "Private-Token: $privateToken" \
    "https://gitlab.com/api/v4/projects?membership=true&order_by=id&sort=asc&per_page=100&last_activity_after=$yesterday" \
   | jq -r '.[] | .id, .path' \
   | while IFS= read projectId; read projectName; do
        echo "Downloading $projectId $projectName"
        curl --header "Private-Token: $privateToken" "https://gitlab.com/api/v4/projects/$projectId/repository/archive.zip" --output "$projectName.zip"
    done

echo All projects downloaded here: $(pwd)
du . -h
echo Copying zips to S3 storage...
mc cp * aux/gitlab/
echo Files moved to aux.lincsproject.ca
echo Cleaning up...
cd ..
rm $dirname -rf
echo Done!
```

## Disaster recovery

There are S3 buckets configured on Compute Canada as well as Scholar's Portal that serve as backup repositories for our backups. The disaster recovery synchronisations are automated to execute once per week at the moment but can also be triggered manually. Just open an SSH session to `ubuntu@aux.lincsproject.ca` and run the `backupAll.sh` script. This script can also be tuned to include or exclude data so that only crucial files are backed up for disaster recovery.

### Google drive backups

The LINCS Google Drive (lincs.project@gmail.com) is backed up to the LINCS S3 [server](https://aux.lincsproject.ca) with [Rclone](https://github.com/rclone/rclone) that is installed on the S3 server. Remote endpoints *gdrive* and *minio* have been configured to connect to the Google drive and S3 server respectively. To copy data from Google to S3, SSH to `ubuntu@aux.lincsproject.ca` and execute an Rclone copy command:

```bash
rclone sync gdrive:/ minio:gdrive -P
```

This command has been added to the `backupAll.sh` so that Google drive data will be copied when the remote backups are triggered.

### The current DR backup script

```bash
cat > backupAll.sh <<EOF
#!/bin/sh
echo 'sync lincs bucket ...'
mc -q mirror lincs/lincs olrc/lincs --overwrite
mc -q mirror lincs/lincs cc/lincs --overwrite
echo 'sync rancher bucket ...'
mc -q mirror lincs/rancher olrc/rancher --overwrite
mc -q mirror lincs/rancher cc/rancher --overwrite
echo 'sync longhorn bucket ...'
mc -q mirror lincs/longhorn olrc/longhorn --overwrite
mc -q mirror lincs/longhorn cc/longhorn --overwrite
echo 'sync Google drive to LINCS S3'
rclone sync gdrive:/ minio:gdrive -P --drive-shared-with-me
echo 'synchronisation complete'
EOF
chmod +x backupAll.sh
```

## Workload Volumes

Workload volumes include all storage volumes mounted to pods in the Kubernetes clusters. The Longhorn UI can be used to backup these volumes manually or to schedule an automated backup to S3 storage. It should be noted also that all *production* volumes have 2 replicas on different nodes by default and should stay online even if one node goes down. The production Longhorn UI can be reached here: [https://longhorn.lincsproject.ca](https://longhorn.lincsproject.ca).

Volumes can be backed up to S3 directly from the Longhorn UI by clicking on a volume and then clicking the *Create Backup* button. It will create a snapshot of the volume and upload that to S3 storage. This can be done while the volume is attached. Also note that snapshots can be taken without creating backups. A specific snapshot can then be backed up to S3 by clicking on the snapshot and selecting *backup*.

A backup schedule can be created for multiple volumes by selecting them on the *Volumes* page and then selecting *Update Schedule* in the menu on the top.

### Restoring a backup

Backups can be restored from the *Backups* tab. You can either choose to restore the backup to the original volume (requires the original volume to be gone), or to create a new recovery volume with an new name. Also use this page to find, delete, and manage backup sets.

The process to restore a backup could look like this:

* Make sure the backup is complete and is visible in the backups list
* Stop the workload that is using the volume to be replaced (if there is one). Simply setting replicas to 0 will work. This will detach the volume.
* Delete the volume and the PVC. Deleting the volume in the Longhorn UI wil automatically delete the PVC.
* Restore the volume from the Backup list menu. Can use a friendly name for the volume. Choose ReadWriteOnce as the Access Mode.
* Back in the Volume list, choose to create PV/PVC for the newly restored volume. Chose to use the previous PVC name.
* Start the workload again in Rancher. It should attach the new volume automatically.

## Wordpress

Wordpress backups are not automated at the moment. The website contains data that is mostly static. It is recommended to backup the site before upgrades are done or any content is changed. Instead of just backing up the storage volume, rather export the site and save the file on S3 storage as is explained in the [docs/lincs_wordpress.md](docs/lincs_wordpress.md) document. This will result in more portable backups that can be restored anywhere.

Currently, we are scheduling a volume backup for the mariadb and wordpress volumes in Longhorn in addition to the site export.

## Kubernetes Cluster configurations

The *production* and *stage* clusters are configured to periodically back up their *etcd* databases to S3 Storage. This was done by editing the cluster configuration in the Rancher UI (from the *Cluster* tab, click the menu icon on the right) and setting the S3 storage parameters in the *Advanced Options* part of the form:

```text
S3 Bucket Name: rancher
S3 Region: us-east-1
S3 Region Endpoint: aux.lincsproject.ca
S3 Folder: production
S3 Access Key: lincs
S3 Secret Key: <the_lincs_key>
```

A manual backup can also be triggered by choosing the *Snapshot Now* option in the cluster menu. Restoring a snapshot is also done through the UI by selecting *Restore Snapshot* in the same menu.

## Rancher backup and restore

Rancher is installed in a K3s Kubernetes cluster on the host. Access to the Rancher VM is obtained with SSH to `ubuntu@rancher.lincsproject.ca`. An authorised SSH certificate must be supplied.

### Built-in storage (SQLite or etcd)

The current LINCS deployment of Rancher uses SQLite storage provided by Rancher. Backing up Rancher is done through the Rancher Backup Operator in the Rancher UI, which has been installed on the LINCS Rancher server. Use the Rancher Explorer and choose Rancher Backups from the main menu on the top left. Click create new backup to create a backup.

Restoring a backup involves creating a k8s cluster, installing the Rancher Backup Operator chart, creating a CRD that points to the backup, and then installing Rancher.

#### 1. Install Kubernetes

Follow the Rancher guide [here](docs/rancher_install.md) to install either k3s or RKE2. Make sure that you use the same version that the backup was made with. Do not install Rancher yet. Official documentation [here](https://rancher.com/docs/rancher/v2.6/en/backups/migrating-rancher).

#### 2. Install Rancher Backup

Be sure to use the same version of rancher-backup that was used to create the backup you want to restore.

```bash
helm repo add rancher-charts https://charts.rancher.io
helm repo update
helm install rancher-backup-crd rancher-charts/rancher-backup-crd \
  -n cattle-resources-system --create-namespace --version 3.0.0
helm install rancher-backup rancher-charts/rancher-backup \
  -n cattle-resources-system --version 3.0.0
```

#### 3. Add the backup to be restored

Create the S3 secret and the CRD that points to the backup file in Minio.

```bash
kubectl -n default create secret generic s3-creds \
  --from-literal=accessKey=lincs --from-literal=secretKey=<the_lincs_key>

cat <<EOF | kubectl apply -f -
apiVersion: resources.cattle.io/v1
kind: Restore
metadata:
  name: restore-migration
spec:
  backupFilename: 2023-03-08-1-23-3-0-6cd5d19d-370d-4e3d-9940-2b204b14b057-2023-03-08T14-45-21Z.tar.gz
  prune: false
  storageLocation:
    s3:
      credentialSecretName: s3-creds
      credentialSecretNamespace: default
      bucketName: rancher
      folder: rancher
      region: us-east-1
      endpoint: aux.lincsproject.ca
EOF

kubectl get restore
kubectl -n cattle-resources-system get pods
kubectl logs -n cattle-resources-system --tail 100 -f rancher-backup-xxx-xxx
```

Once the Restore resource has the status `Completed`, you can continue the Rancher installation. May take a few minutes.

#### 4. Install Rancher

Follow the Rancher guide [here](docs/rancher_install.md) to install Rancher into the cluster. It will restore the backup automatically.

### MySQL storage

Previous versions of LINCS Rancher deployment used a MySQL database to store the K3s database instead of etcd, which makes it relatively easy to backup and restore. Rancher can be completely restored with just the MySQL backup.

Here is a script that can be scheduled to back up the Rancher database weekly and then send it to our S3 storage. It requires that the Minio `mc` client is installed and configured:

```bash
cat > backup-k3s.sh <<EOF
#!/bin/sh
NOW=$(date +"%y-%m-%d")
mysqldump -u debian-sys-maint -p9VIA8XQivi0XqAZ1 --host=localhost --opt --skip-lock-tables --single-transaction \
        --verbose --hex-blob --routines --triggers --all-databases > ~/k3s-backup-$NOW.sql
gzip k3s-backup-$NOW.sql
~/mc cp ~/k3s-backup-$NOW.sql.gz lincs/rancher/
rm ~/k3s-backup-$NOW.sql.gz
echo "k3s-backup-$NOW.sql.gz sent to lincs/rancher"
EOF
chmod +x backup-k3s.sh
```

### Restoring Rancher

This process basically involves restoring the MySQL database and pointing K3s to the database when installing it. Take care to use the same parameters that was used when it was installed initially as specified in the Rancher installation guide here: [rancher_install.md](docs/rancher_install.md).

### Useful MySQL admin commands

Get the password for the `debian-sys-maint` user:

```bash
sudo cat /etc/mysql/debian.cnf
```

Stream a dump directly to S3 storage:

```bash
mysqldump -u root -p=lincs_ftw --host=localhost --opt --skip-lock-tables --single-transaction \
        --verbose --hex-blob --routines --triggers --all-databases | ./mc pipe lincs/rancher/k3sdump-2020-7.sql
```

Example restore of a database:

```bash
mysql -u debian-sys-maint -p9VIA8XQivi0XqAZ1 -e "SELECT @@datadir;"
mysqladmin -u debian-sys-maint -p9VIA8XQivi0XqAZ1 create kubernetes
mysql -u debian-sys-maint -p9VIA8XQivi0XqAZ1 kubernetes < k3sdump-2020-7.sql
```

### Reset the MySQL password

Here are the basic steps. Adjust as needed.

```bash
sudo /etc/init.d/mysql stop
sudo mkdir /var/run/mysqld
sudo chown mysql:mysql /var/run/mysqld
sudo mysqld_safe --skip-grant-tables &
mysql -uroot
use mysql;
update user set authentication_string=PASSWORD("lincs_password") where User='root';
update user set authentication_string=PASSWORD("9VIA8XQivi0XqAZ1") where User='debian-sys-maint';
//SET PASSWORD FOR 'root'@'localhost' = PASSWORD('lincs_password');
flush privileges;
quit
sudo /etc/init.d/mysql stop
sudo reboot
mysql -u root -p=lincs_password
```
