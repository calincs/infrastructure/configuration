# Maintenance tasks

All tasks and configurations that require manual intervention are listed here.

## Resize persistent volume claims (PVC)

If the pvc is managed by Helm, it is as simple as changing the requested volume size in the pvc template. The Longhorn provider should manage everything automatically. Note that you can only size up, and not down.

If the pvc is not managed by Helm, e.g., the volume is backed up by Longhorn and we manually restored it at least once, then we have to follow the following procedure to expand the volume:

1. Backup the current volume
1. Uninstall the chart that manages the deployment. It should not remove the PVC if it isn't managed by Helm.
1. If the pvc was removed, just restore it from backup and create the pvc for the volume from the Longhorn UI.
1. Resize the pvc. The easiest way is to use the Rancher UI: it has a resize option on the context menu of the pvc.
1. Change the pvc template in the application repository to represent the exact same size.
1. Run the pipeline to redeploy the application.

## Defrag Etcd databases

The etcd database of a cluster can get defragmented to the point where an alert shows up in monitoring. This can be remedied by defragging the etcd database. Set the kubectl context and then use the following command to defrag all etcd nodes in the cluster:

```bash
kubectl -n kube-system exec $(kubectl -n kube-system get pod -l component=etcd --no-headers -o custom-columns=NAME:.metadata.name | head -1) -- sh -c "ETCDCTL_ENDPOINTS='https://127.0.0.1:2379' ETCDCTL_CACERT='/var/lib/rancher/rke2/server/tls/etcd/server-ca.crt' ETCDCTL_CERT='/var/lib/rancher/rke2/server/tls/etcd/server-client.crt' ETCDCTL_KEY='/var/lib/rancher/rke2/server/tls/etcd/server-client.key' ETCDCTL_API=3 etcdctl defrag --cluster"
```

Simply restarting one of the etcd nodes in the cluster will also resolve the issue, because RKE2 defrags the etcd database on startup.

## Manual backups

### Volume backups

Storage volumes are not backed up automatically. Selected volumes can be scheduled for backup with the Longhorn UI.

### Wordpress

Follow the guide here: [docs/lincs_wordpress.md](docs/lincs_wordpress.md)

## Off-site backups

To trigger a DR backup, execute the `backupAll.sh` script on **aux.lincsproject.ca**. Includes the lincs.project Google Drive sync. These backups are currently scheduled to be executed weekly with a cron job.

## Upgrades and Updates

### Wordpress and Plugins

Go to [https://lincsproject.ca/admin](https://lincsproject.ca/admin), log in, and click on the update lincs.

### Upgrade Rancher

```bash
sudo apt-get update
sudo apt-get upgrade
sudo reboot
sudo helm repo update
sudo helm fetch rancher-latest/rancher
sudo helm upgrade rancher rancher-latest/rancher --namespace cattle-system --set hostname=rancher.lincsproject.ca --set ingress.tls.source=letsEncrypt --set letsEncrypt.email=zacanbot@gmail.com
sudo kubectl -n cattle-system scale --replicas 1 deploy/rancher
sudo kubectl -n cattle-system rollout status deploy/rancher
```

### K8s node (VM) upgrades

### Upgrade Kubernetes applications

### Cluster upgrades

### Runner updates

### Minio Backup server

There is a renew script on the Minio server that should automatically renew certificates for our backup server. But it has happened in the past that a server update and restart rendered the backup server inoperable. This was caused by out-dated certificates. The certbot renew also didn't work.

This problem was fixed by requesting new certificates with a DNS challenge, since Minio does not support the HTTP01 mechanism. This involves going through the whole procedure again that is documented the [minio_s3_storage.md](minio_s3_storage.md) guide.
