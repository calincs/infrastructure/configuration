# Infrastructure

A project repository to manage the LINCS infrastructure on Compute Canada. The repo contains guides and configurations of software in the LINCS ecosystem.

1. [Compute Canada resources](docs/compute_canada.md)
2. [Rancher installation](docs/rancher_install.md)
3. [Creating a new cluster](docs/create_cluster.md)
4. [Setting up Kubectl](docs/setup_kubectl.md)
5. [LINCS Kubernetes storage on Compute Canada](docs/storage_guide.md)
6. [Ingress configuration](docs/ingress_config.md)
7. [GitLab integration](docs/gitlab_integration.md)
8. [Adding and maintaining k8s nodes](docs/node_maintenance.md)
9. [Minio S3 block storage for LINCS](docs/minio_s3_storage.md)
10. [LINCS Backups](docs/backups.md)
11. [ChartMuseum: The LINCS Helm repo](docs/chart_museum.md)
12. [The LINCS website (Wordpress)](docs/lincs_wordpress.md)
13. [Maintenance tasks](docs/maintenance.md)
14. [Vault Setup](docs/vault_setup.md)
15. [Using Vault in GitLab](docs/vault_usage.md)
16. [Keycloak and RabbitMQ](docs/keycloak_rabbit.md)
17. [Jitsi-Meet](docs/jitsi.md)
18. [JupyterHub](docs/jupyterhub)
19. [Elastic Stack](docs/elastic.md)

## OpenLens

OpenLens is a desktop IDE that one can use to manage k8s clusters. Download [here](https://github.com/MuhammedKalkan/OpenLens/releases). Add the pod menu extension by following the instructions [here](https://github.com/alebcay/openlens-node-pod-menu).
